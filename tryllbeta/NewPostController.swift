//
//  CameraDetailController.swift
//  tryllbeta
//
//  Created by Surabhi chavan on 06/08/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import GooglePlaces
import Photos

class PhotoLibraryDetailController: UIViewController {
    
    var countryName = ""
    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var sliderStepBar: G8SliderStep!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UILabel!
    @IBOutlet weak var priceSlider: TryllSlider!
    @IBOutlet var circleButtons: [TryllFilterCircleButton]!
    @IBOutlet var filters: [TryllFilterButton]!
    @IBOutlet weak var searchPlace: UIButton!
    @IBOutlet weak var subtitleTextField: UITextField!
    @IBOutlet weak var headlineTextView: UITextView!
    @IBOutlet weak var ambienceStackView: UIStackView!
    @IBOutlet weak var occasionStackView: UIStackView!
    @IBOutlet weak var priceRangeStackView: UIStackView!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var textFieldCustomTag: UITextField!
    @IBOutlet weak var labelFriendsCount: UILabel!
    
    @IBOutlet weak var textFieldCustomOccasionTag: UITextField!
    @IBOutlet var emojiImageView: UIImageView!
    var catagories : [String] = []
    var likes : [String] = []
    var chosenFilters :[String] = []
    var taggedFirends : [[String:Any]] = [[:]]
    weak var place: GMSPlace?
    var pickedImage : UIImage?
    var selectedImages : [PHAsset] = []
    var lattitude : String = ""
    var longitude : String = ""
    var isFromPhotoLibrary = Bool()
    let firestoreRef = Firestore.firestore()
    let storageRef = Storage.storage().reference()
    var currentUserID = ""
    var currentUserName = ""
    var imagePath = [String]()
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButton(_ sender: Any) {
//        checkForCorrectInput()
//        startUploading()
        
        
        if (self.placeAddressLabel.text == "To find your desired location" || self.placeNameLabel?.text == "Use the magnifying glass") {
            removeSpinner()
            showAlert(title: "You seem to have forgotten something", message: "Please fill the place name field", completion: nil)
            return
        }
        
        showSpinner(onView: self.submitButton)
        
        chosenFilters.removeAll()
        catagories.removeAll()
        circleButtons.forEach({
            $0.isClicked ? chosenFilters.append($0.buttonTitle) : ()
            $0.isClicked ? catagories.append($0.buttonTitle) : ()
        })
        filters.forEach({
            $0.isClicked ? chosenFilters.append($0.buttonTitle) : ()
        })
        
        
        
        
        guard let uid = Auth.auth().currentUser?.uid else {
            removeSpinner()
            
            showAlert(title: "Error", message: "Please make sure you are logged in", completion: nil)
            return
        }
        
        var username = ""
        self.currentUserID = uid
        
        firestoreRef.collection("Users").document(uid).getDocument(completion: { (snap, error) in
            let dict = snap?.data()
            
            username = dict?["username"] as? String ?? ""
            self.currentUserName = username
        })
        
        
        
        
        //let img = imageView!.image!
        var countImage = 0
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        
        if isFromPhotoLibrary == true {
            for image in self.selectedImages {
                
                let path = firestoreRef.collection("Trylls").document().path
                let imageRef = storageRef.child("\(path)\(myStringafd)/image.jpg")
                var img = UIImage()
                if isFromPhotoLibrary == true {
                     img = self.getAssetThumbnail(asset: image)
                } else {
                    img = self.pickedImage!
                }
                
                
                // Convert Image into data and prepare for upload
                if let imageData = img.jpegData(compressionQuality: 1.0){
                    //                self.showSpinner(onView: self.view)
                    // Uploads the image
                    let uploadTask = imageRef.putData(imageData)
                    
                    // Observe upload progress
                    uploadTask.observe(.progress) { (snapshot) in
                        
                        let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)
                        print("Completed: \(percentComplete)")
                        if percentComplete == 100.00 {
                            print(countImage)
                        }
                    }
                    
                    uploadTask.observe(.failure) { (snapshot) in
                        self.removeSpinner()
                        self.showAlert(title: "Error", message: "Could not upload, please check your internet connection", completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                    
                    
                    // If the upload is successful
                    uploadTask.observe(.success) { (snapshot) in
                        //self.removeSpinner()
                        self.imagePath.append(imageRef.fullPath)
                        countImage = countImage + 1
                        if self.isFromPhotoLibrary == true {
                             if countImage == self.selectedImages.count {
                                 self.uploadDataAfterImageUpload()
                             }
                        } else {
                            self.uploadDataAfterImageUpload()
                        }
                    }
                }
            }
        } else {
                let path = firestoreRef.collection("Trylls").document().path
                let imageRef = storageRef.child("\(path)\(myStringafd)/image.jpg")
                var img = UIImage()
                img = self.pickedImage!

                // Convert Image into data and prepare for upload
            if let imageData = img.jpegData(compressionQuality: 1.0){
                    //                self.showSpinner(onView: self.view)
                    // Uploads the image
                    let uploadTask = imageRef.putData(imageData)
                    
                    // Observe upload progress
                    uploadTask.observe(.progress) { (snapshot) in
                        
                        let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)
                        print("Completed: \(percentComplete)")
                        if percentComplete == 100.00 {
                            print(countImage)
                        }
                    }
                    
                    uploadTask.observe(.failure) { (snapshot) in
                        self.removeSpinner()
                        self.showAlert(title: "Error", message: "Could not upload, please check your internet connection", completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                    
                    
                    // If the upload is successful
                    uploadTask.observe(.success) { (snapshot) in
                        
                        self.imagePath.append(imageRef.fullPath)
                            self.uploadDataAfterImageUpload()
                    }
                }
            
        }
        
        
        

        
        
    }
    
    
    private func uploadDataAfterImageUpload()  {
        let path = firestoreRef.collection("Trylls").document().path
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        let customTags = self.textFieldCustomTag.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        var customTagArray : [String] = []
        if customTags.count > 0 {
            customTagArray = customTags.components(separatedBy: ",")
        }
        
        for tags in customTagArray {
            self.chosenFilters.append(tags)
        }
        
        let customOccasionTags = self.textFieldCustomOccasionTag.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        var customOccasionTagArray : [String] = []
        if customOccasionTags.count > 0 {
            customOccasionTagArray = customOccasionTags.components(separatedBy: ",")
        }
        
        for tags in customOccasionTagArray {
            self.chosenFilters.append(tags)
        }
        
        self.removeSpinner()
        firestoreRef.document(path).setData([
            "timestamp": myStringafd,
            "catagories": self.catagories,
            //                        "createdAt" : calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date),
            "emojiRating" : self.sliderStepBar.value,
            "price_range" : self.priceSlider.value,
            "images" : self.imagePath,
            "subtitle": self.headlineTextView.text ?? "",
            "headline": self.subtitleTextField.text ?? "",
            //                        "created_by": firestoreRef.collection("Users").document(uid),
            "created_by_uid" : currentUserID,
            "username" : currentUserName,
            "place_name" : self.placeNameLabel.text!,
            "location" : [
                "country": self.countryName,
                "address": self.placeAddressLabel.text!
            ],
            "tags" : self.chosenFilters,
            "tagged_friends" : self.taggedFirends,
            "likes" : self.likes
            ], merge: true)
        
        // Post the data with new reassigned location for the picture to the database
        //                    ref.child("Store").child(newPostKey).setValue(formValues)
        print("Post uploaded")
        self.dismiss(animated: true, completion: nil)
        
        self.showAlert(title: "Succesful post", message: "You just posted a tryll, Thank you for being part of our test team 👏", completion: nil)
    }
    
    
    @IBAction func buttonTagFriendAction(_ sender: Any) {
        self.view.endEditing(true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FriendsToTagView") as! FriendsToTagView
        controller.block = { dict in
            let positionValue = dict
            let position_name = positionValue[0]["username"] as! String
            self.taggedFirends = dict
            self.labelFriendsCount.text = "\(self.taggedFirends.count) friends"
            print("positionValue---",position_name)
        }
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: {
        })
    }
    @IBAction func searchLocation(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.addressComponents.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    fileprivate func checkForCorrectInput()  {
        if (self.placeAddressLabel.text == "Use the magnifying glass" || self.placeNameLabel?.text == "To find your desired location") {
            showAlert(title: "You seem to have forgotten something", message: "Please fill the place name field", completion: nil)
            removeSpinner()
            return
        }
        
        chosenFilters.removeAll()
        catagories.removeAll()
        circleButtons.forEach({
            $0.isClicked ? chosenFilters.append($0.buttonTitle) : ()
            $0.isClicked ? catagories.append($0.buttonTitle) : ()
        })
        filters.forEach({
            $0.isClicked ? chosenFilters.append($0.buttonTitle) : ()
        })
    }
    
    fileprivate func startUploading(){
        let firestoreRef = Firestore.firestore()
        let storageRef = Storage.storage().reference()
        
        
            
            guard let uid = Auth.auth().currentUser?.uid else {
                showAlert(title: "Error", message: "Please make sure you are logged in", completion: nil)
                return
            }
            
            var username = ""
            firestoreRef.collection("Users").document(uid).getDocument(completion: { (snap, error) in
                let dict = snap?.data()
                
                username = dict?["username"] as? String ?? ""
            })
            
            
            let path = firestoreRef.collection("Trylls").document().path
            let imageRef = storageRef.child("\(path)/image.jpg")
            
            
            let img = imageView!.image!
            // Convert Image into data and prepare for upload
            if let imageData = img.jpegData(compressionQuality: 0.8){
//                self.showSpinner(onView: self.view)
                // Uploads the image
                let uploadTask = imageRef.putData(imageData)
                
                // Observe upload progress
                uploadTask.observe(.progress) { (snapshot) in
                    
                    let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)
                    print("Completed: \(percentComplete)")
                }
                
                uploadTask.observe(.failure) { (snapshot) in
                    self.removeSpinner()
                    self.showAlert(title: "Error", message: "Could not upload, please check your internet connection", completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                
                
                // If the upload is successful
                uploadTask.observe(.success) { (snapshot) in
                    self.removeSpinner()
                    firestoreRef.document(path).setData([
                        "timestamp": Date(),
                        "catagories": self.catagories,
//                        "createdAt" : calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date),
                        "emojiRating" : self.sliderStepBar.value,
                        "price_range" : self.priceSlider.value,
                        "images" : imageRef.fullPath,
                        "subtitle": self.headlineTextView.text ?? "",
                        "headline": self.subtitleTextField.text ?? "",
//                        "created_by": firestoreRef.collection("Users").document(uid),
                        "created_by_uid" : uid,
                        "username" : username,
                        "place_name" : self.placeNameLabel.text!,
                        "location" : [
                            "country": self.countryName,
                            "address": self.placeAddressLabel.text!
                        ],
                        "tags" : self.chosenFilters
                        ], merge: true)
                    
                    // Post the data with new reassigned location for the picture to the database
//                    ref.child("Store").child(newPostKey).setValue(formValues)
                    print("Post uploaded")
                    self.dismiss(animated: true, completion: nil)
                    
                    self.showAlert(title: "Succesful post", message: "You just posted a tryll, Thank you for being part of our test team 👏", completion: nil)
                
            }
        }
    }
    
    fileprivate func configureSlider() {
        sliderStepBar.stepImages =   [#imageLiteral(resourceName: "e1"), #imageLiteral(resourceName: "e2"), #imageLiteral(resourceName: "e3"), #imageLiteral(resourceName: "e4"), #imageLiteral(resourceName: "e5")].map({$0.scale(to: CGSize(width: 50, height: 50 ))!})
        sliderStepBar.minimumValue = 4
        sliderStepBar.maximumValue = Float(sliderStepBar.stepImages!.count) + sliderStepBar.minimumValue - 1.0
        sliderStepBar.stepTickColor = UIColor.clear
        sliderStepBar.stepTickWidth = 0.2
        sliderStepBar.stepTickHeight = 10
        sliderStepBar.trackHeight = 0.3
        sliderStepBar.value = 5
         sliderStepBar.addTarget(self, action: #selector(setEmojiImage), for: [.touchUpInside, .touchUpOutside, .touchCancel])
        }
        
        @objc func setEmojiImage() {
            emojiImageView.image = sliderStepBar.currentThumbImage
        }
    
    fileprivate func configureSubviews() {
        view.backgroundColor = UIColor(white: 0.99, alpha: 1)
        //imageView.image = image
        submitButton.layer.cornerRadius = .tryllBorderRadius
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //configureSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoCollectionView.reloadData()
        for button in circleButtons {
            button.delegate = self
        }
        configureSlider()
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: self.view.frame.size.width, height: 300), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
}

// Handle the user's selection.
extension PhotoLibraryDetailController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        self.place = place
        
//        self.searchPlace.setTitle(place.name, for: .normal)
        self.countryName = (place.addressComponents?.first(where: {$0.type == "country" })!.name)!
        self.placeNameLabel.text = place.name
        self.placeAddressLabel.text = place.formattedAddress
        self.lattitude = String(place.coordinate.latitude)
        self.longitude = String(place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


extension CGFloat{
    static var tryllBorderRadius: CGFloat {
        return CGFloat(6)
    }
}



var vSpinner : UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

extension PhotoLibraryDetailController: TryllFilterCircleButtonDelegate {
    func hideSelectedStackViews(buttonTitle: String, isClicked: Bool) {
        if isClicked {
        if buttonTitle == "Landmark" {
            self.ambienceStackView.isHidden = true
            self.occasionStackView.isHidden = true
            self.priceRangeStackView.isHidden = true
        } else if buttonTitle  == "Shopping" {
            self.ambienceStackView.isHidden = false
            self.priceRangeStackView.isHidden = false
            self.occasionStackView.isHidden = true
        }
        } else {
            self.ambienceStackView.isHidden = false
            self.priceRangeStackView.isHidden = false
            self.occasionStackView.isHidden = false
        }
    }
}
extension PhotoLibraryDetailController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromPhotoLibrary == true {
            return selectedImages.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCollectionCell", for: indexPath) as! PhotoCollectionCell
        if isFromPhotoLibrary == true  {
            let image = self.getAssetThumbnail(asset: self.selectedImages[indexPath.item])
            cell.photo.image = image
        } else {
            cell.photo.image = self.pickedImage!
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
//    {
//        return 30.0
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
//    {
//        return 30.0
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
//    {
//
//    }
}

