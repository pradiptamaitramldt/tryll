//
//  LogInViewController.swift
//  tryllbeta
//
//  Created by Tryll on 7/2/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseCrashlytics
import FirebaseAuth
import Firebase
import SAConfettiView



class LogInViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var dismissButton: UIButton!
    
    var confettiView: SAConfettiView!
    
    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dismissButton.addTarget(self, action: #selector(handleSignIn), for: .touchUpInside)
        emailField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        passwordField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        
        
        
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.type = .Image(UIImage(named: "white1")!)
        confettiView.colors = [UIColor.tryllWhite, UIColor.tryllYellow, UIColor.black]
        self.view.addSubview(confettiView)
        confettiView.startConfetti()
        confettiView.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        if userDefault.bool(forKey: "usersignedin"){
//            performSegue(withIdentifier: "toHomescreen", sender: self)
//        }
       
    }
    

    @objc func textFieldChanged(_ target:UITextField) {
//        let email = emailField.text
//        let password = passwordField.text
//        let formFilled = email != nil && email != "" && password != nil && password != ""
       
    }
    
    @objc func handleSignIn() {
         //fatalError()
        guard let email = emailField.text else { return }
        guard let pass = passwordField.text else { return }
        
       
        showSpinner(onView: view)
        Auth.auth().signIn(withEmail: email, password: pass) { user, error in
            if error == nil && user != nil {
                self.userDefault.set(true, forKey: "usersignedin")
                self.userDefault.synchronize()
                self.removeSpinner()
                self.performSegue(withIdentifier: "toHomefeed", sender: self)
                
            } else {
                print("Error logging in: \(error!.localizedDescription)")
                self.showAlert(title: "Error", message: "\(error!.localizedDescription)", completion: nil)
            }
            self.removeSpinner()
            
}
        
        func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            
            if Auth.auth().currentUser != nil {
                self.performSegue(withIdentifier: "toHomefeed", sender: self)
            }
        }
        
}
}
