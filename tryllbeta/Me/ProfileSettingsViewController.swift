//
//  ProfileSettingsViewController.swift
//  tryllbeta
//
//  Created by Tryll on 7/16/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseUI

class ProfileSettingsViewController: UIViewController {

    @IBOutlet weak var profileSwitch: UISwitch!
    @IBOutlet weak var switchAlert: UISwitch!
    var userName = ""
    var firstName = ""
    var lastName = ""
    var emailID = ""
    var bioDescription = ""
    var profileType = "Public"
    var phoneNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let uid = Auth.auth().currentUser?.uid  else{
            self.showAlert(title: "Error happened", message: "Failed at downloading your data, Reason: You are not logged in,", completion: nil)
            return
        }
        print("uid \(uid)")
        
        Firestore.firestore().collection("Users").document(uid).getDocument { (snap, error) in
            guard snap?.data() != nil else{return}
            
            //            guard snap.isSome else {return}
            
            
            print(snap?.data())
            
            let dict = snap?.data() as! [String:Any]
            print(dict["email"] as? String ?? "")
            self.userName = dict["username"] as? String ?? ""
            self.lastName = dict["last_name"] as? String ?? ""
            self.firstName = dict["first_name"] as? String ?? ""
            self.emailID = dict["email"] as? String ?? ""
            self.phoneNumber = dict["phone"] as? String ?? ""
            self.bioDescription = dict["bio"] as? String ?? ""
            self.profileType = dict["profileType"] as? String ?? "Public"
            
            if self.profileType == "Public" {
                       self.profileSwitch.setOn(true, animated: true)
            } else {
                       self.profileSwitch.setOn(false, animated: true)
            }
        }
        
    
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func showAlertWithEmailid() {
        let alertController = UIAlertController(title: "Please enter your register email", message: nil, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Submit", style: .default) { (_) in
            if let txtField = alertController.textFields?.first, let text = txtField.text {
                // operations
                print("Text==>" + text)
                
                if self.isValidEmail(testStr: text) {
                    self.resetPasswordLink(withemail: text)
                    
                } else {
                    self.showAlert(title: "Error", message: "Please enter a valid email", completion: nil)
                    //self.toast(message: "Please enter a valid email")
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (_) in }
        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func resetPasswordLink(withemail email : String) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            print("Reset Request sent")
            self.alertPressed(message: "Please check your registered email")
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
    }
    
    private func alertPressed(message: String)
    {
        let alertController = UIAlertController(title: "success", message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { action in
            
        }))
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonResetPasswordAction(_ sender: Any) {
        showAlertWithEmailid()
    }
    @IBAction func buttonConnectToFBAction(_ sender: Any) {
    }
    @IBAction func buttonConnectToGoogleAction(_ sender: Any) {
    }
    @IBAction func switchAction(_ sender: UISwitch) {
    }
    
    @IBAction func profileSwitchAction(_ sender: UISwitch) {
        if profileSwitch.isOn {
            print("ON")
            showProfileAlert(withAlertType: "Public")
        } else {
            print("OFF")
            showProfileAlert(withAlertType: "Private")
        }
    }
    
    @IBAction func buttonLogoutAction(_ sender: Any) {
        let defaults = UserDefaults.standard
        do{
            try Auth.auth().signOut()
        }catch{
            fatalError(error as! String)
        }
        
        defaults.set(false, forKey: "usersignedin")
        let vc = storyboard?.instantiateViewController(withIdentifier: "toLogIn") as! LogInViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func showProfileAlert(withAlertType alertType : String) {
        
        var message = ""
        if alertType == "Public" {
            message = "in this way everyone will be able to see your content. Are you ok with this?"
        } else {
            message = "in this way only friends whose request you accept, will be able to see your content. Are you ok with this?"
        }
        
        let alert = UIAlertController(title: "Profile", message: message,         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            if self.profileSwitch.isOn {
                self.profileSwitch.setOn(false, animated: true)
            } else {
                self.profileSwitch.setOn(true, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.profileType = alertType
                                        self.updateProfile(withProfileType: alertType)
                                        
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func updateProfile(withProfileType profileTypeFinal : String) {
        
        guard let uid = Auth.auth().currentUser?.uid  else{
            self.showAlert(title: "Error happened", message: "Failed at downloading your data, Reason: You are not logged in,", completion: nil)
            return
        }
        
//        Firestore.firestore().collection("Users").document(uid).updateData([
//            "username" : self.userName,
//            "first_name" : self.firstName,
//            "last_name" : self.lastName,
//            "email" : self.emailID,
//            "bio" : self.bioDescription,
//            "profileType" : profileTypeFinal
//        ])
        
        let userObject : [String:Any] = [
            "uid":uid,
            "username": self.userName,
            "profile_picture": "user/\(uid)/profile_photo.jpg",
            "first_name" : self.firstName,
            "last_name" : self.lastName,
            "phone" : "",
            "email" : self.emailID,
            "bio" : self.bioDescription,
            "profileType": profileTypeFinal
            ]
        
        Firestore.firestore().collection("Users").document(uid).setData(userObject)
        
        
        //Firestore.firestore().collection("Users").document(uid).set
        
    }
    
}



