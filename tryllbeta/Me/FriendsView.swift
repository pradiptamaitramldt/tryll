//
//  FriendsView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 27/03/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseUI
import FirebaseAuth
import FirebaseFirestore

class FriendsView: UIViewController {

    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var labelNoUserFound: UILabel!
    
    var isFollowingSelected : Bool = false
    var users: [[String: Any]] = [[:]]
    var filteredUsers: [[String: Any]] = [[:]]
    var followed: [String] =  []
    var followers: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.segmentControll.selectedSegmentIndex=0
        self.textFieldSearch.delegate=self
        self.table_view.delegate=self
        self.table_view.dataSource=self
        self.labelNoUserFound.isHidden = true
        UISegmentedControl.appearance().setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor(netHex: 0xFFCB27)], for: .selected)
        self.textFieldSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        users.removeAll()
        filteredUsers.removeAll()
        followed.removeAll()
        followers.removeAll()
        self.loadFollowersUsers()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.filterGroupkList(filterString: textField.text!)
    }
    
    func filterGroupkList(filterString: String) {
        self.filteredUsers = self.users
        if filterString.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.filteredUsers = self.filteredUsers.filter {
                let username = $0["username"] as? String
                return username?.range(of: filterString, options: .caseInsensitive) != nil
            }
        }
        self.table_view.reloadData()
        return
    }
    
    func loadFollowingUsers() {
        DispatchQueue.main.async { Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
            snap?.documents.forEach({ (user) in
                user.data().mapValues({
                    self.followed.append($0 as! String)
                })
            })
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.forEach({ (user) in
                    if user.data().count != 0 {
                        let uid = user.data()["uid"] as! String
                        if self.followed.count == 0{
                            self.labelNoUserFound.isHidden = false
                        }else{
                            self.labelNoUserFound.isHidden = true
                        }
                        if self.followed.contains(uid) {
                            self.users.append(user.data())
                            self.filteredUsers.append(user.data())
                        }
                    }
                })
                self.table_view.reloadData()
            }
        })
        }
    }
    
    func loadFollowersUsers() {
        DispatchQueue.main.async { Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Followers").getDocuments(completion: { (snap, err) in
            snap?.documents.forEach({ (user) in
                user.data().mapValues({
                    self.followers.append($0 as! String)
                })
            })
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.forEach({ (user) in
                    if user.data().count != 0{
                        let uid = user.data()["uid"] as! String
                        if self.followers.count == 0{
                            self.labelNoUserFound.isHidden = false
                        }else{
                            self.labelNoUserFound.isHidden = true
                        }
                        if self.followers.contains(uid) {
                            self.users.append(user.data())
                            self.filteredUsers.append(user.data())
                        }
                        self.table_view.reloadData()
                    }
                })
            }
        })
            Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
                snap?.documents.forEach({ (user) in
                    user.data().mapValues({
                        self.followed.append($0 as! String)
                    })
                })
            })
        }
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex==0 {
            

            users.removeAll()
            filteredUsers.removeAll()
            followed.removeAll()
            followers.removeAll()
            self.isFollowingSelected = false
            self.loadFollowersUsers()
        }else{
            users.removeAll()
            filteredUsers.removeAll()
            followed.removeAll()
            followers.removeAll()
            self.isFollowingSelected = true
            self.loadFollowingUsers()
        }
    }
    
    @IBAction func buttonSearchAction(_ sender: Any) {
    }
    
}
extension FriendsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredUsers.count != 0 {
            return filteredUsers.count
        }
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendListTableCell") as! FriendListTableCell
        
        if filteredUsers.count != 0 {
            if let userPhotoRef = filteredUsers[indexPath.row]["profile_picture"]{
                let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
                cell.friendProfileIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            }
            
            let uid = filteredUsers[indexPath.row]["uid"] as! String
            cell.userUID = uid
            
            cell.labelFriendName.text = filteredUsers[indexPath.row]["username"] as? String ?? "..."
            cell.isFollowed = followed.contains(uid)
        } else {
            if let userPhotoRef = users[indexPath.row]["profile_picture"]{
                let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
                cell.friendProfileIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            }
            
            let uid = users[indexPath.row]["uid"] as! String
            cell.userUID = uid
            
            cell.labelFriendName.text = users[indexPath.row]["username"] as? String ?? "..."
            cell.isFollowed = followed.contains(uid)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Message", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
               //vc.hidesBottomBarWhenPushed = true
        vc.reciverID = filteredUsers[indexPath.row]["uid"] as! String
        vc.reciverImageLink = filteredUsers[indexPath.row]["profile_picture"] as! String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
extension FriendsView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
