//
//  EditProfileViewController.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/14/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseUI

class EditProfileViewController: UIViewController {
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var userNameTextFeild: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var bioTextField: UITextField!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    var imagePicker: UIImagePickerController!
    
    @IBAction func cancelButtonClickdd(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitButtonClicked(_ sender: Any) {
        
        guard let uid = Auth.auth().currentUser?.uid  else{
            self.showAlert(title: "Error happened", message: "Failed at downloading your data, Reason: You are not logged in,", completion: nil)
            return
        }
        
        
        let img = profilePictureImageView.image!.jpegData(compressionQuality: 0.8)
        let metaData = StorageMetadata()
        metaData.contentType = "image/JPG"
        
        Firestore.firestore().collection("Users").document(uid).updateData([
            "username" : userNameTextFeild.text as! String,
            "first_name" : firstNameTextField.text as! String,
            "last_name" : lastNameTextField.text as! String,
            "email" : emailTextField.text as! String,
            "bio" : bioTextField.text as! String
        ])
        Storage.storage().reference().child("user").child(uid).child("profile_photo.jpg").putData(img!, metadata: metaData) { (metadata, error) in
            if error == nil {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showAlert(title: "Error", message: "Failed to update profile. Please try again later.", completion: nil)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addImageButton.addTarget(self, action: #selector(openImagePicker), for: .touchUpInside)
        addImageButton.layer.cornerRadius = addImageButton.bounds.width/2
        
        
        imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self 
        
        
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.bounds.width/2
        
        guard let uid = Auth.auth().currentUser?.uid  else{
            self.showAlert(title: "Error happened", message: "Failed at downloading your data, Reason: You are not logged in,", completion: nil)
            return
        }
        DispatchQueue.main.async {
            Storage.storage().reference(withPath: "user/\(uid)/profile_photo.jpg").getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error)
                } else {
                    let image = UIImage(data: data!)
                    self.profilePictureImageView.image = image
                    self.profilePictureImageView.contentMode = .scaleAspectFill
                }
            }
        }
        self.profilePictureImageView.contentMode = .scaleAspectFill
        
        
        Firestore.firestore().collection("Users").document(uid).getDocument { (snap, error) in
            guard snap?.data() != nil else{return}
            
            //            guard snap.isSome else {return}
            
            
            print(snap?.data())
            
            let dict = snap?.data() as! [String:Any]
            self.userNameTextFeild.text = dict["username"] as? String ?? ""
            self.firstNameTextField.text = dict["first_name"] as? String ?? ""
            self.lastNameTextField.text = dict["last_name"] as? String ?? ""
            self.emailTextField.text = dict["email"] as? String ?? ""
            //            self.phoneNumberTextField.text = dict["phone_number"] as! String ?? ""
            self.bioTextField.text = dict["bio"] as? String ?? ""
            
        }
    }
    
    @objc func openImagePicker(_ sender:Any){
        self.present(imagePicker, animated: true, completion: nil)
    }
    
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let pickedImage = info[.editedImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        profilePictureImageView.image = pickedImage
        
        
        picker.dismiss(animated: true, completion: nil)
    }
}
