//
//  PostListView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 27/03/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class PostListView: UIViewController {

    @IBOutlet weak var postCollectionView: UICollectionView!
    static var feedArray : [[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.postCollectionView.delegate=self
        self.postCollectionView.dataSource=self
    }
    
    @objc func TotallikeClicked(sender:UIButton)
    {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FollowSuggestionViewController") as! FollowSuggestionViewController
        vc.fromFeeds = true
        
        if let likes = PostListView.feedArray[sender.tag]["likes"] as? [String] {
            vc.likeUser  = likes
        } else {
            vc.likeUser = []
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PostListView: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PostListView.feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeedsCollectionViewCell
        if PostListView.feedArray.count != 0 {
            DispatchQueue.main.async {
                
                let item = PostListView.self.feedArray[indexPath.row]
                //cell.delegate = self
                cell.createdByUID = item["created_by_uid"] as? String ?? ""
                //cell.postId = self.postsIds[indexPath.row]
                let imageArray = item["images"] as! [String]
                let firstImage =  imageArray[0] as String
                let imgRef = Storage.storage().reference().child(firstImage)
                cell.postedImage.sd_setImage(with: imgRef, placeholderImage: #imageLiteral(resourceName: "Group"))
                cell.postedImage.contentMode = .scaleAspectFill
                cell.suitcaseButton.isUserInteractionEnabled = false
//                if self.suitcasedTrylls[indexPath.row] {
//                    cell.suitcaseButton.setImage(UIImage(named: "suitcase_selected"), for: .normal)
//                } else {
//                    cell.suitcaseButton.setImage(UIImage(named: "suitcase_unselected"), for: .normal)
//                }
                
                let profileImgRef = Storage.storage().reference().child("user").child(item["created_by_uid"] as! String).child("profile_photo.jpg")
                cell.profileImage.sd_setImage(with: profileImgRef)
                cell.timeStamp.text = item["timestamp"] as? String ?? ""
                cell.username.text = item["username"] as? String
                let location =  item["location"] as! [String:String]
                cell.cityLabel.text = location["address"]
                cell.postedImageLocation.text = item["place_name"] as? String
                cell.postedImageDescription.text = item["headline"] as? String
                cell.postedImageDescription.numberOfLines=0
                cell.postedImageDescription.lineBreakMode = .byCharWrapping
                cell.postedImageDescription.sizeToFit()
                cell.timeStamp.text =   item["timestamp"] as? String
                cell.postedImageReaction.image = updateEmojiRatingCatagory(item["emojiRating"] as! Int)
                let emojiRating = item["emojiRating"] as! Int
                
//                switch emojiRating {
//                case 4:
//                    cell.postedImageDescription.textColor=UIColor(red: 172.0/255, green: 8.0/255, blue: 0, alpha: 1.0)
//                case 5:
//                    cell.postedImageDescription.textColor=UIColor(red: 250.0/255, green: 78.0/255, blue: 68.0/255, alpha: 1.0)
//                case 6:
//                    cell.postedImageDescription.textColor=UIColor(red: 250.0/255, green: 107.0/255, blue: 1.0/255, alpha: 1.0)
//                case 7:
//                    cell.postedImageDescription.textColor=UIColor(red: 252.0/255, green: 168.0/255, blue: 3.0/255, alpha: 1.0)
//                case 8:
//                    cell.postedImageDescription.textColor=UIColor(red: 253.0/255, green: 203.0/255, blue: 40.0/255, alpha: 1.0)
//                default:
//                    cell.postedImageDescription.textColor=UIColor(red: 253.0/255, green: 203.0/255, blue: 40.0/255, alpha: 1.0)
//                }
                
                
                cell.likeButton.tag = indexPath.row
                 if let totallike = item["likes"] as? [String]{
                     print("total likes \(totallike.count)")
                     cell.likeCount = totallike.count
                     cell.totalLikeCountBtn.setTitle(String(totallike.count), for: .normal)
                     
                     if totallike.count > 0 {
                         if totallike.contains(Auth.auth().currentUser?.uid ?? "") {
                             print("indexpath \(indexPath.row) like section")
                             cell.islikeButtonOn = true
                             cell.likeButton.setImage(UIImage(named:"like"), for: .normal)
                             //cell.likeButton.addTarget(self,action:#selector(self.unlikeClicked), for:.touchUpInside)
                             
                         } else {
                             print("indexpath \(indexPath.row) unlike section")
                             cell.islikeButtonOn = false
                             cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                             //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                         }
                     } else {
                         cell.likeCount = 0
                         cell.islikeButtonOn = false
                         cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                         cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                     }
                     
                     
                                    
                 } else {
                     cell.likeCount = 0
                     cell.islikeButtonOn = false
                     cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                     cell.totalLikeCountBtn.setTitle("", for: .normal)
                     //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                 }
                 cell.totalLikeCountBtn.tag = indexPath.row
                 
                cell.totalLikeCountBtn.addTarget(self,action:#selector(self.TotallikeClicked), for:.touchUpInside)
                 
                 if let totalComments = item["comments"] as? [[String : Any]] {
                     cell.totalCommentsBtn.setTitle(String(totalComments.count), for: .normal)
                 } else {
                     cell.totalCommentsBtn.setTitle("", for: .normal)
                 }
                
                
                
                if let tags = item["tags"] as? [String]{
                    cell.postedImageCategory.image = updateImageCatagory(tags)
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 520)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
