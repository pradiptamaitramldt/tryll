//
//  ResetPaswordView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 31/03/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseUI

class ResetPaswordView: UIViewController {

    @IBOutlet weak var textFieldOldPassword: UITextField!
    @IBOutlet weak var textFieldNewPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonSaveAction(_ sender: Any) {
        if self.textFieldOldPassword.text != "" {
            if self.textFieldNewPassword.text != "" {
                if self.textFieldConfirmPassword.text != "" {
                    if self.textFieldNewPassword.text == self.textFieldConfirmPassword.text {
                        let password = self.textFieldNewPassword.text!
                        
                        if Auth.auth().currentUser != nil {
                          // User is signed in.
                          // ...
                        } else {
                          // No user is signed in.
                          // ...
                        }
                        Auth.auth().currentUser?.updatePassword(to: password) { (error) in
                            if error == nil {
                                self.showAlert(title: "Success", message: "Password changed successfully", completion: nil)
                            }else{
                                self.showAlert(title: "Error", message: "\(error!.localizedDescription)", completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
}
