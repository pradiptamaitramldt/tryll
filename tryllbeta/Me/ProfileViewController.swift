//
//  ProfileViewController.swift
//  tryllbeta
//
//  Created by Tryll on 7/15/19.
//  Copyright © 2019 Tryll. All rights reserved.
//
import UIKit
import Mapbox
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseUI

class ProfileViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var followersCount: UIButton!
    @IBOutlet weak var followingCount: UIButton!
    @IBOutlet weak var postCount: UIButton!
    @IBOutlet weak var leftNavBarButton: UIBarButtonItem!
    @IBOutlet weak var buttonEditProfile: UIButton!
    
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var editProfileButton: UIBarButtonItem!
    @IBOutlet weak var profileNavigationHeader: UINavigationItem!
    
    var posts: [[String:Any]] = []
    var postsIds: [String] = []
    var postsIdMapping: [Timestamp:String] = [:]
    var postsIdMapping2: [String:String] = [:]
    var countriesArray : [String] = []
    var userId = ""
    var isFollowed = false
    var userData = [String : Any]()
    var dateTimeArray = [String]()
    
    @IBOutlet weak var mapView: MGLMapView!
    @IBAction func underDevelopmentAlert(_ sender: Any) {
        if self.userId.isEmpty {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileSettingsViewController") as! ProfileSettingsViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func fetchDataFromFirebase(){
        var uid = self.userId
        if uid.isEmpty {
            //My Profile
            uid = Auth.auth().currentUser!.uid
            leftNavBarButton.title = ""
            followButton.isHidden = true
            buttonEditProfile.isHidden = false
            editProfileButton.title = "Edit"
            editProfileButton.isEnabled = false
            self.profileNavigationHeader.title = "My Profile"
            leftNavBarButton.setBackgroundImage(UIImage(named: "settings_24px"), for: .normal, barMetrics: .default)
            
        } else {
            //Other User's Profile
            followButton.isHidden = false
            editProfileButton.title = ""
            buttonEditProfile.isHidden = true
            editProfileButton.isEnabled = true
            leftNavBarButton.title = ""
            leftNavBarButton.setBackgroundImage(UIImage(named: "exit"), for: .normal, barMetrics: .default)
            self.profileNavigationHeader.title = ""
        }
        
        DispatchQueue.main.async {
            Storage.storage().reference(withPath: "user/\(uid)/profile_photo.jpg").getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error)
                } else {
                    let image = UIImage(data: data!)
                    self.profilePictureImageView.image = image
                    self.profilePictureImageView.contentMode = .scaleAspectFill
                }
            }
            
            let fireStoreRef = Firestore.firestore()
            
            fireStoreRef.collection("Users").document(uid).getDocument { (snap, error) in
                let dict = snap?.data()
                let user_id = Auth.auth().currentUser!.uid
                if uid != user_id {
                    self.userData = dict!
                    self.usernameLabel.text = "\(dict?["username"] as? String ?? "No username") profile"
                }else{
                    self.usernameLabel.text = dict?["username"] as? String ?? "No username"
                }
                
                self.bioLabel.text = dict?["bio"] as? String ?? ""
            }
            
            
            fireStoreRef.collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments { (snap, err) in
                if err == nil {
                    self.followingCount.setTitle("\(snap!.count)", for: .normal)
                } else{
                    self.followingCount.setTitle("0", for: .normal)
                }
                
                var followingList: [String] = []
                snap?.documents.forEach({ (user) in
                    user.data().mapValues({
                        followingList.append($0 as! String)
                        
                    })
                })
                self.isFollowed = followingList.contains(self.userId)
                self.followButton.backgroundColor = self.isFollowed ? .tryllBlack :  .tryllYellow
                self.followButton.setTitle(self.isFollowed ?  "Followed" : "Follow", for: .normal)
                self.followButton.setTitleColor(self.isFollowed ? .tryllYellow : .tryllBlack , for: .normal)
                self.followButton.titleLabel?.font = self.isFollowed ? UIFont.systemFont(ofSize: 15, weight: .bold) : UIFont.systemFont(ofSize: 15, weight: .regular)
            }
            
            
            fireStoreRef.collection("Users").document(uid).collection("Followers").getDocuments { (snap, err) in
                if err == nil {
                    self.followersCount.setTitle("\(snap!.count)", for: .normal)
                }else{
                    self.followersCount.setTitle("0", for: .normal)
                }
                
            }
            
            
            fireStoreRef.collection("Trylls").whereField("created_by_uid", isEqualTo: uid).getDocuments { (snap, err) in
                self.postCount.setTitle("\(snap!.count)", for: .normal)
                snap?.documents.forEach({ (postInfo) in
                    let post = postInfo.data()
                    let documentId = postInfo.documentID
                    let timeStampClass = String(describing: type(of: post["timestamp"]!))
                    print("class...\(timeStampClass)")
                    if timeStampClass == "FIRTimestamp" {
                        self.postsIdMapping[(post["timestamp"] as? Timestamp)!] = documentId
                    }else{
                        self.postsIdMapping2[(post["timestamp"] as? String)!] = documentId
                    }
                    print("time stamp \(post["timestamp"] as? String)")
                    self.posts.append(post)
                    
                    self.dateTimeArray.append(post["timestamp"] as! String)
                   
                   
                    
                    let df = DateFormatter()
                    df.dateFormat = "dd-MM-yyyy hh:mm:ss"
                    self.posts = self.posts.sorted {df.date(from: $0["timestamp"] as! String)! > df.date(from: $1["timestamp"] as! String)!}
                    
                    //print("shorted array \(sortedArray)")
                    
                    
                    self.updateCountries(dict: self.posts)
                    
                    self.collectionView.reloadData()
                })
                //self.posts = self.posts.sorted(by: { ($0["timestamp"] as? Timestamp)!.seconds > ($1["timestamp"] as? Timestamp)!.seconds })
                for post in self.posts {
                    let timeStampClass = String(describing: type(of: post["timestamp"]!))
                    print("class...\(timeStampClass)")
                    if timeStampClass == "FIRTimestamp" {
                        self.postsIds.append(self.postsIdMapping[(post["timestamp"] as? Timestamp)!]!)
                    }else{
                        self.postsIds.append(self.postsIdMapping2[(post["timestamp"] as? String)!]!)
                    }
                }
                //self.posts.reverse()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    override func viewDidAppear(_ animated: Bool) {
        leftNavBarButton.setBackgroundImage(UIImage(named: ""), for: .normal, barMetrics: .default)
        posts.removeAll()
        postsIds.removeAll()
        countriesArray.removeAll()
        self.mapView.reloadStyle(countriesArray)
        self.collectionView.reloadData()
        fetchDataFromFirebase()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        mapView.delegate = self
        mapView.logoView.isHidden = true
        mapView.centerCoordinate  = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        mapView.styleURL = URL(string: "mapbox://styles/mapbox/streets-v11")
        profilePictureImageView.layer.cornerRadius = 40
    }
    
    @IBAction func showPostsPage(_ sender: Any) {
        PostListView.feedArray = self.posts
    }
    
    @IBAction func showFollowingPage(_ sender: Any) {
        if isFollowed==true {
            FollowSuggestionViewController.followIndex = 1
        }
    }
    
    
    @IBAction func showFollowersPage(_ sender: Any) {
        if isFollowed==true {
            FollowSuggestionViewController.followIndex = 2
        }
    }
    @IBAction func messageAction(_ sender: Any) {
        if self.userId.isEmpty {
            
        } else {
            if isFollowed {
                let storyboard = UIStoryboard(name: "Message", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
                //vc.hidesBottomBarWhenPushed = true
                vc.reciverID = userId
                vc.reciverImageLink = self.userData["profile_picture"] as! String
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func followButtonPressed(_ sender: UIButton) {
        if isFollowed {
            UnfollowPerson()
        } else {
            followPerson()
        }
        followButton.backgroundColor = isFollowed ? .tryllYellow :  .tryllBlack
        followButton.setTitle(isFollowed ?  "Follow" : "Followed", for: .normal)
        followButton.setTitleColor(isFollowed ? .tryllBlack : .tryllYellow , for: .normal)
        followButton.titleLabel?.font = isFollowed ? UIFont.systemFont(ofSize: 15, weight: .regular) : UIFont.systemFont(ofSize: 15, weight: .bold)
        self.isFollowed = !self.isFollowed
    }
    
    func followPerson() {
        let usersRef = Firestore.firestore().collection("Users")
        usersRef.document(Auth.auth().currentUser!.uid).collection("Following").addDocument(data: [
            "user_Id" : self.userId
            ])
        usersRef.document(self.userId).collection("Followers").addDocument(data: [
            "user_Id" : Auth.auth().currentUser!.uid
            ])
    }
    
    func UnfollowPerson(){
        let usersRef = Firestore.firestore().collection("Users")
        
        usersRef.document(Auth.auth().currentUser!.uid).collection("Following").whereField("user_Id", isEqualTo: self.userId).getDocuments { (snap, err) in
            snap?.documents.forEach({ (snapshot) in
                snapshot.reference.delete()
            })
        }
        
        usersRef.document(self.userId).collection("Followers").whereField("user_Id", isEqualTo: Auth.auth().currentUser!.uid).getDocuments { (snap, err) in
            snap?.documents.forEach({ (snapshot) in
                snapshot.reference.delete()
            })
        }
    }
    
    public func updateCountries(dict: [[String:Any]]){
        dict.forEach { (post) in
            let l = post["location"] as! [String:Any]
            self.countriesArray.append(l["country"]! as! String)
        }
        mapView.reloadStyle(countriesArray)
    }
}

extension ProfileViewController: MGLMapViewDelegate{
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        print("Did Finish Loading style")
        
        if !countriesArray.isEmpty{
            let source = MGLVectorTileSource(identifier: "ne_10m_admin_0_countries-6i91ur", configurationURL: URL(string: "mapbox://surabhi27.cjn9q7la")!)
            style.addSource(source)
            
            // Creating a layer to color
            let layer = MGLFillStyleLayer(identifier: "abc", source: source)
            let layer2 = MGLLineStyleLayer(identifier: "abcd", source: source)
            
            // Layer Source
            layer.sourceLayerIdentifier = "ne_10m_admin_0_countries-6i91ur"
            layer2.sourceLayerIdentifier = "ne_10m_admin_0_countries-6i91ur"
            
            style.addLayer(layer)
            style.addLayer(layer2)
            
            var coutriesString = ""
            countriesArray.forEach { (c) in
                if c == "United States" {coutriesString.append("(NAME == 'United States of America') || ")}
                else{coutriesString.append("(NAME == '\(c)') || ")}
            }
            //            mapView.fly(to: MGLMapCamera(, completionHandler: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
            // Reforamte countires trying by erasing the last four space and delete " || "
            coutriesString = String(coutriesString.dropLast(4))
            
            
            layer.predicate = NSPredicate(format: coutriesString)
            layer2.predicate = NSPredicate(format: coutriesString)
            layer.fillColor = NSExpression(forConstantValue: UIColor(displayP3Red: 255/255, green: 203/255, blue: 39/255, alpha: 1.0))
            layer2.lineColor = NSExpression(forConstantValue: UIColor.tryllCountrySeperator)
        }
    }
}

let colors :[UIColor] = [.blue, .red]

extension ProfileViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FullImageViewController") as! FullImageViewController
        self.navigationController?.navigationBar.tintColor = .tryllBlack
        
        let item = posts[indexPath.row]
        let itemId = postsIds[indexPath.row]
        let imageArray = posts[indexPath.row]["images"] as! [String]
        let firstImage =  imageArray[0] as String
        let imgRef = Storage.storage().reference().child(firstImage)
        //        vc.post = imgRef
        vc.postData = item
        vc.postId = itemId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postsCell", for: indexPath) as! PostCollectionViewCell
        
        let imageArray = posts[indexPath.row]["images"] as! [String]
        let firstImage =  imageArray[0] as String
        
        let s = Storage.storage().reference().child(firstImage)
        cell.imageView.sd_setImage(with: s, placeholderImage: #imageLiteral(resourceName: "logo"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (self.view.frame.width / 3) - 1
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
