//
//  FriendsToTagView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 06/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseUI
import FirebaseAuth
import FirebaseFirestore

class FriendsToTagView: UIViewController {
    
    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var labelNoUserFound: UILabel!
    @IBOutlet weak var buttonDone: UIButton!
    
    var isFollowingSelected : Bool = false
    var users: [[String: Any]] = [[:]]
    var filteredUsers: [[String: Any]] = [[:]]
    var followed: [String] =  []
    var followers: [String] = []
    
    var block: (([[String : Any]])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.segmentControll.selectedSegmentIndex=0
        self.textFieldSearch.delegate=self
        self.table_view.register(UINib(nibName: "WDPeopleCell", bundle: nil), forCellReuseIdentifier: "wdPeopleCell")
        self.table_view.allowsMultipleSelection=true
        self.table_view.delegate=self
        self.table_view.dataSource=self
        self.labelNoUserFound.isHidden = true
        UISegmentedControl.appearance().setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor(netHex: 0xFFCB27)], for: .selected)
        self.textFieldSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        users.removeAll()
        filteredUsers.removeAll()
        followed.removeAll()
        followers.removeAll()
        self.loadFollowersUsers()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.filterGroupkList(filterString: textField.text!)
    }
    
    func filterGroupkList(filterString: String) {
        self.filteredUsers = self.users
        if filterString.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.filteredUsers = self.filteredUsers.filter {
                let username = $0["username"] as? String
                return username?.range(of: filterString, options: .caseInsensitive) != nil
            }
        }
        self.table_view.reloadData()
        return
    }
    
    func loadFollowingUsers() {
        DispatchQueue.main.async { Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
            snap?.documents.forEach({ (user) in
                user.data().mapValues({
                    self.followed.append($0 as! String)
                })
            })
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.forEach({ (user) in
                    if user.data().count != 0 {
                        let uid = user.data()["uid"] as! String
                        if self.followed.count == 0{
                            self.labelNoUserFound.isHidden = false
                        }else{
                            self.labelNoUserFound.isHidden = true
                        }
                        if self.followed.contains(uid) {
                            self.users.append(user.data())
                            self.filteredUsers.append(user.data())
                        }
                    }
                })
                self.table_view.reloadData()
            }
        })
        }
    }
    
    func loadFollowersUsers() {
        DispatchQueue.main.async { Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Followers").getDocuments(completion: { (snap, err) in
            snap?.documents.forEach({ (user) in
                user.data().mapValues({
                    self.followers.append($0 as! String)
                })
            })
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.forEach({ (user) in
                    if user.data().count != 0{
                        let uid = user.data()["uid"] as! String
                        if self.followers.count == 0{
                            self.labelNoUserFound.isHidden = false
                        }else{
                            self.labelNoUserFound.isHidden = true
                        }
                        if self.followers.contains(uid) {
                            self.users.append(user.data())
                            self.filteredUsers.append(user.data())
                        }
                        self.table_view.reloadData()
                    }
                })
            }
        })
            Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
                snap?.documents.forEach({ (user) in
                    user.data().mapValues({
                        self.followed.append($0 as! String)
                    })
                })
            })
        }
    }
    
    @IBAction func buttonDoneAction(_ sender: Any) {
        var taggedFriends : [[String:Any]] = [[:]]
        if let selectedIndexes = self.table_view.indexPathsForSelectedRows {
            taggedFriends.removeAll()
            for indexPath in selectedIndexes {
                let friend = self.filteredUsers[indexPath.row]
                taggedFriends.append(friend)
            }
            block?(taggedFriends)
            self.dismiss(animated: true, completion: nil)
        }
    }
        
    @IBAction func buttonBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
//        if let indexPath = table_view.indexPathForSelectedRow{
//            let data = dictData[dictKeys[indexPath.section]]![indexPath.row]
//            block?(data)
//            if self.navigationController == nil{
//                self.dismiss(animated: true, completion: nil)
//            }
//            self.dismiss(animated: true, completion: nil)
//        }
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex==0 {
            
            
            users.removeAll()
            filteredUsers.removeAll()
            followed.removeAll()
            followers.removeAll()
            self.isFollowingSelected = false
            self.loadFollowersUsers()
        }else{
            users.removeAll()
            filteredUsers.removeAll()
            followed.removeAll()
            followers.removeAll()
            self.isFollowingSelected = true
            self.loadFollowingUsers()
        }
    }
    
    @IBAction func buttonSearchAction(_ sender: Any) {
    }
    
}
extension FriendsToTagView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredUsers.count != 0 {
            return filteredUsers.count
        }
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wdPeopleCell", for: indexPath) as! WDPeopleCell
        
        if filteredUsers.count != 0 {
            if let userPhotoRef = filteredUsers[indexPath.row]["profile_picture"]{
                let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
                cell.userImageIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            }
            cell.lblUserName.text=filteredUsers[indexPath.row]["username"] as? String ?? "..."
        } else {
            if let userPhotoRef = users[indexPath.row]["profile_picture"]{
                let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
                cell.userImageIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            }
            
            cell.lblUserName.text = users[indexPath.row]["username"] as? String ?? "..."
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}
extension FriendsToTagView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
