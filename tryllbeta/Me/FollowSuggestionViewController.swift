//
//  FollowSuggestionViewController.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/16/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseUI
import FirebaseAuth
import FirebaseFirestore

class FollowSuggestionViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating {
    
    
    var users: [[String: Any]] = [[:]]
    var filteredUsers: [[String: Any]] = [[:]]
    var followed: [String] =  []
    var followers: [String] = []
    
    var likeUser:[String] = []
    var fromFeeds = false
    
    static var followIndex = 0
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users.removeAll()
        filteredUsers.removeAll()
        
        tableView.separatorStyle = .none
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.delegate = self
        
        if fromFeeds == true {
            self.likeUsers()
        } else {
            if FollowSuggestionViewController.followIndex == 0 {
                self.loadAllUsers()
            }
            else if FollowSuggestionViewController.followIndex == 1 {
                self.loadFollowingUsers()
            }
            else if FollowSuggestionViewController.followIndex == 2 {
                self.loadFollowersUsers()
            }
        }
        
        
    }
    
    func likeUsers() {
        print("only liked user")
        print("like array \(likeUser)")
       // uHPwKDjJ6hedfsLn67zyPg7e8Mn1
        for user in likeUser {
            if user != Auth.auth().currentUser!.uid {
                Firestore.firestore().collection("Users").document(user).getDocument { (snap, error) in
                    guard snap?.data() != nil else{return}
                    
                    //            guard snap.isSome else {return}
                    print(snap?.data())
                    let dict = snap?.data() as! [String:Any]
                     self.users.append(dict)
                    self.filteredUsers.append(dict)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    
    func loadAllUsers() {
        
        DispatchQueue.main.async {
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.map({ (user) in
                    self.users.append(user.data() )
                    self.filteredUsers.append(user.data())
                    
                    self.tableView.reloadData()
                })
            }
            Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
                snap?.documents.map({ (user) in
                    user.data().mapValues({
                        self.followed.append($0 as! String)
                        self.tableView.reloadData()
                    })
                })
            })
            
        }
        
    }
    
    func loadFollowingUsers() {
        DispatchQueue.main.async { Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
            snap?.documents.map({ (user) in
                user.data().mapValues({
                    self.followed.append($0 as! String)
                })
            })
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.map({ (user) in
                    let uid = user.data()["uid"] as! String
                    if self.followed.contains(uid) {
                        self.users.append(user.data())
                        self.filteredUsers.append(user.data())
                    }
                })
                self.tableView.reloadData()
            }
        })
        }
    }
    
    func loadFollowersUsers() {
        DispatchQueue.main.async { Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Followers").getDocuments(completion: { (snap, err) in
            snap?.documents.map({ (user) in
                user.data().mapValues({
                    self.followers.append($0 as! String)
                })
            })
            Firestore.firestore().collection("Users").getDocuments { (snap, err) in
                snap?.documents.map({ (user) in
                    let uid = user.data()["uid"] as! String
                    if self.followers.contains(uid) {
                        self.users.append(user.data())
                        self.filteredUsers.append(user.data())
                    }
                    self.tableView.reloadData()
                })
            }
        })
            Firestore.firestore().collection("Users").document(Auth.auth().currentUser!.uid).collection("Following").getDocuments(completion: { (snap, err) in
                snap?.documents.map({ (user) in
                    user.data().mapValues({
                        self.followed.append($0 as! String)
                    })
                })
            })
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredUsers.removeAll()
        for value in users {
            let username = value["username"] as? String
            if (username?.contains(searchText))! {
                filteredUsers.append(value)
            }
        }
        
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredUsers.count != 0 {
            return filteredUsers.count
        }
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "followSuggestionCell") as! FollowTableViewCell
        
        if filteredUsers.count != 0 {
            if let userPhotoRef = filteredUsers[indexPath.row]["profile_picture"]{
                let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
                cell.avatarImageView.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            }
            
            let uid = filteredUsers[indexPath.row]["uid"] as! String
            cell.userUID = uid
            
            cell.userNameLabel.text = filteredUsers[indexPath.row]["username"] as? String ?? "..."
            cell.isFollowed = followed.contains(uid)
        } else {
            if let userPhotoRef = users[indexPath.row]["profile_picture"]{
                let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
                cell.avatarImageView.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            }
            
            let uid = users[indexPath.row]["uid"] as! String
            cell.userUID = uid
            
            cell.userNameLabel.text = users[indexPath.row]["username"] as? String ?? "..."
            cell.isFollowed = followed.contains(uid)
        }
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let uid = users[indexPath.row]["uid"] as! String
//        if followed.contains(uid) == false {
//            print(followed.contains(uid))
//        } else {
//            print(followed.contains(uid))
//            let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//            vc.userId = filteredUsers[indexPath.row]["uid"] as! String
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.userId = filteredUsers[indexPath.row]["uid"] as! String
        self.navigationController?.pushViewController(vc, animated: true)

    }
}

