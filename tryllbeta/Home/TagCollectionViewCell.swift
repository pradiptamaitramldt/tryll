//
//  TagCollectionViewCell.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 17/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTagName: UILabel!
}
