//
//  FullImageViewController.swift
//  tryllbeta
//
//  Created by Ashwini Prabhu on 7/16/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseUI
import FirebaseFirestore
import FirebaseAuth
import SwiftyJSON

class fullImageCollectioViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImageView: UIImageView!
    
}


public func updateEmojiRatingCatagory(_ int:Int) -> UIImage {
    let n : UIImage.Emoji = ._3
    print(int)
    if int == 4 {return UIImage(fromAssets: ._1)}
    if int == 5 {return UIImage(fromAssets: ._2)}
    if int == 6 {return UIImage(fromAssets: ._3)}
    if int == 7 {return UIImage(fromAssets: ._4)}
    if int == 8 {return UIImage(fromAssets: ._5)}
    
    return UIImage(fromAssets: n)
}

public func updateImageCatagory(_ array: [String]) -> UIImage {
    
    if array.contains("food".capitalized) {return UIImage(fromAssets: .food)}
    if array.contains("landmark".capitalized) {return UIImage(fromAssets: .landmark)}
    if array.contains("entertainment".capitalized) {return UIImage(fromAssets: .entertainment)}
    if array.contains("drink".capitalized) {return UIImage(fromAssets: .drink)}
    if array.contains("hotel".capitalized) {return UIImage(fromAssets: .hotel)}
    if array.contains("shopping".capitalized) {return UIImage(fromAssets: .shopping)}
    return UIImage()
}

class FullImageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //    var post: StorageReference!
    var postData: [String:Any]!
    var postId = ""
    var createdByUID: String = ""
    
    var commentsArray = [Comment]()
    
    @IBOutlet weak var emojiRatingImageView: UIImageView!
    @IBOutlet weak var imagePosted: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var imagePostedDescription: UILabel!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var otherImageDescription: UILabel!
    @IBOutlet weak var CommentsLabel: UILabel!
    @IBOutlet weak var addCommentField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tagsStackView: UIStackView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    
    @IBOutlet var commentsTableView: UITableView!
    @IBOutlet weak var deletePostButton: UIButton!
    
    var tagArray : [String] = []
    var imageArray = [String]()
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    //Comments
    
    fileprivate func updatePostInformation() {
        imageArray = postData["images"] as! [String]
        let imageArray = postData["images"] as! [String]
        let firstImage =  imageArray[0] as String
        
        let imgRef = Storage.storage().reference().child(firstImage)
        //        self.imagePosted.sd_setImage(with: imgRef, placeholderImage: #imageLiteral(resourceName: "Group"))
        //imagePosted.contentMode = .scaleAspectFill
        imagePosted.sd_setImage(with: imgRef)
        pageControl.numberOfPages = imageArray.count
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.reloadData()
        
        
        profileImage.sd_setImage( with: Storage.storage().reference().child("user").child(postData["created_by_uid"] as! String).child("profile_photo.jpg"))
        self.createdByUID = postData["created_by_uid"] as? String ?? ""
        
        username.text = postData["username"] as? String
        let location =  postData["location"] as! [String:String]
        self.location.text = postData["place_name"] as? String
        self.cityLabel.text = location["address"]
        self.cityLabel.sizeToFit()
        otherImageDescription.text = postData["subtitle"] as? String ?? ""
        otherImageDescription.sizeToFit()
        imagePostedDescription.text = postData["headline"] as? String
        imagePostedDescription.numberOfLines=0
        imagePostedDescription.lineBreakMode = .byCharWrapping
        imagePostedDescription.sizeToFit()
        
        emojiRatingImageView.image = updateEmojiRatingCatagory(postData["emojiRating"] as! Int)
        if self.createdByUID != Auth.auth().currentUser?.uid {
            self.deletePostButton.isHidden = true
        } else {
            self.deletePostButton.isHidden = false
        }
        self.tagArray = (postData["tags"] as? [String])!
        if self.tagArray.count > 0 {
            self.tagCollectionView.reloadData()
        }
        if let tags = postData["tags"] as? [String]{
            imageCategory.image = updateImageCatagory(tags)
            if !tags.isEmpty{
                tags.forEach{
                    let label = UILabel()
                    label.text = $0
                    label.font = UIFont.systemFont(ofSize: 11)
                    label.numberOfLines = 0
                    label.backgroundColor = .white
                    label.textAlignment = .center
                    label.layer.cornerRadius = 1
                    label.layer.borderWidth = 1
                    label.layer.borderColor = UIColor.tryllYellow.cgColor
                    label.adjustsFontSizeToFitWidth = true
                    
                    tagsStackView.insertArrangedSubview(label, at: 0)
                }
            }
        }
        
        let postTimeStr = postData["timestamp"] as? String
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        let postTime = formatter.date(from: postTimeStr ?? "")
        
       
        timestamp.text = postTime?.timeAgoDisplay()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImage.layer.cornerRadius = profileImage.bounds.height / 2
        emojiRatingImageView.transform = CGAffineTransform(rotationAngle: 1/12 * .pi)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        username.isUserInteractionEnabled = true
        username.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openProfile)))
        updatePostInformation()
        fetchComments()
        commentsTableView.dataSource = self
        commentsTableView.delegate = self
        self.tagsStackView.isHidden=true
        self.tagCollectionView.delegate=self
        self.tagCollectionView.dataSource=self
        sendButton.addTarget(self, action: #selector(sendComment), for: .touchUpInside)
        
        //listen to user input on comment text field
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
    @objc func openProfile() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.userId = createdByUID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchComments() {
        //Request to fetch comments
        commentsArray.removeAll()
        var commentsData: [String : Any] = [:]
        let docRef = Firestore.firestore().collection("Trylls").document(self.postId)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                commentsData = (document.data())!
                let json = JSON(commentsData["comments"] as Any)
                
                for (key,subJson):(String, JSON) in json {
                    
                    Firestore.firestore().collection("Users").document(subJson["userId"].stringValue).getDocument { (userdocument, error) in
                        
                        let userData = (userdocument!.data())!
                        
                        let json = JSON(userData)
                        
                        let storf = Storage.storage().reference().child("user").child(json["uid"].stringValue).child("profile_photo.jpg").getData(maxSize: 1 * 1024 * 1024) { (data, error) -> Void in
                            // Create a UIImage, add it to the array
                            if error == nil {
                                let pic = UIImage(data: data!)
                                self.commentsArray.append(Comment(userImage: pic!, userName: json["username"].stringValue, comment: subJson["comment"].stringValue, timestamp: subJson["timestamp"].int!))
                                DispatchQueue.main.async {
                                    self.commentsArray = self.commentsArray.sorted(by: { $0.timestamp > $1.timestamp })

                                    self.commentsTableView.reloadData()
                                }
                            }
                        }
                    }
                }
                
            } else {
                print("Document does not exist")
            }
        }
    }
    
    @objc func sendComment() {
        if !addCommentField.text!.isEmpty {
            Firestore.firestore().collection("Trylls").document(self.postId).updateData([
                "comments": FieldValue.arrayUnion([["userId" : Auth.auth().currentUser?.uid ?? "", "comment" :self.addCommentField.text!, "timestamp" : Timestamp(date: Date()).seconds]])
            ]){   err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                    self.fetchComments()
                    
                }
            }
            
            addCommentField.text = ""
        }
        
    }
    
    @IBAction func deletePostPressed(_ sender: UIButton) {
        Firestore.firestore().collection("Trylls").document(self.postId).delete { (error) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentTableViewCell
        
        cell?.userProfileImageView.image = commentsArray[indexPath.row].userImage
        cell?.commentLabel.text = commentsArray[indexPath.row].comment
        cell?.userName.text = commentsArray[indexPath.row].userName
        cell?.timeStampLabel.text = Date(timeIntervalSince1970: Double(commentsArray[indexPath.row].timestamp)).timeAgoSinceDate(numericDates: true)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //stop listening to user input on comment text field
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    var oldNav: UINavigationBar!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        self.navigationController?.navigationBar.isTranslucent = false
        //        oldNav = self.navigationController!.navigationBar
        //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationBar.isTranslucent = true
        //         setNavigationBarHidden(false, animated:true)
        //        self.navigationController?.navigationBar.tintColor = .tryllYellow
        
        //        self.navigationController?.presentTransparentNavigationBar()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.hideTransparentNavigationBar()
        //        self.navigationController?.navigationBar.(oldNav.backgroundColor, for: <#T##UIBarPosition#>, barMetrics: <#T##UIBarMetrics#>) =
        //        self.navigationController?.navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: .default), for: .default)
        //        self.navigationController?.navigationBar.isTranslucent  = UINavigationBar.appearance().isTranslucent
        //        self.navigationController?.navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
        //        self.navigationController?.navigationBar.
        //        self.navigationController?.navigationBar.isTranslucent = false
        //        self.navigationController?.navigationBar.tintColor = oldNav.tintColor
    }
    
    // raise keyboard along with comment field when user clicks on comment text field
    @objc func handleKeyboardNotification(notification: NSNotification){
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else{
            return
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification ||
            notification.name == UIResponder.keyboardWillChangeFrameNotification{
            view.frame.origin.y = -keyboardFrame.height
        }
        else{
            view.frame.origin.y = 0
        }
    }
    
}

extension UINavigationController {
    public func presentTransparentNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        navigationBar.tintColor = .tryllWhite
        //        setNavigationBarHidden(false, animated:true)
    }
    
    public func hideTransparentNavigationBar() {
        //        setNavigationBarHidden(true, animated:false)
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: .default), for:.default)
        navigationBar.isTranslucent = false
        //
        navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
    }
}


extension FullImageViewController: UITextFieldDelegate{
    
    // resign first responder for comment textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addCommentField.resignFirstResponder()
        return true
    }
}


extension UIImage{
    enum Assests:String{
        case food = "Food"
        case landmark = "Landmark"
        case entertainment = "Entertainment"
        case drink = "Drink"
        case hotel = "Hotel"
        case shopping = "Shopping"
        static let values = [food,landmark,entertainment,drink,hotel,shopping]
        
    }
    
    enum Emoji: String {
        case _1 = "e1"
        case _2 = "e2"
        case _3 = "e3"
        case _4 = "e4"
        case _5 = "e5"
    }
    
    
    convenience init!(fromAssets : Assests) {
        self.init(named: fromAssets.rawValue)
    }
    convenience init!(fromAssets : Emoji) {
        self.init(named: fromAssets.rawValue)
    }
    
}

struct Comment {
    let userImage: UIImage
    let userName: String
    let comment: String
    let timestamp: Int
}


extension Date {
    func timeAgoSinceDate(numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(self as Date)
        let latest = (earliest == now as Date) ? self : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
}
extension FullImageViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == imageCollectionView {
            return self.imageArray.count
        }
        
        return self.tagArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == imageCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fullImageCollectioViewCell", for: indexPath) as! fullImageCollectioViewCell
            let firstImage =  imageArray[indexPath.row] as String
            
            let imgRef = Storage.storage().reference().child(firstImage)
            print("image link \(imgRef)")
            //cell.mainImageView.contentMode = .scaleAspectFit
            cell.mainImageView.sd_setImage(with: imgRef)
    
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCollectionViewCell", for: indexPath) as! TagCollectionViewCell
        cell.labelTagName.text = self.tagArray[indexPath.item]
        cell.contentView.borderWidth = 1.0
        cell.contentView.borderColor = .gray
        //cell.labelTagName.sizeToFit()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == tagCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as? TagCollectionViewCell
            let label = UILabel(frame: CGRect.zero)
            label.text = self.tagArray[indexPath.item]
            label.font = cell?.labelTagName.font
            label.sizeToFit()
            return CGSize(width: label.frame.width, height: 30)
        }
        return CGSize(width: self.view.frame.size.width, height: 250)
       }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let cell = collectionView.cellForItem(at: indexPath) as? TagCollectionViewCell
//
//        return CGSize(width: (cell?.labelTagName.bounds.width ?? 0)+20, height: 30)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == imageCollectionView {
            return 0.0
        }
        return 2.0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.imageCollectionView {
            let visibleRect = CGRect(origin: self.imageCollectionView.contentOffset, size: self.imageCollectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            if let visibleIndexPath = self.imageCollectionView.indexPathForItem(at: visiblePoint) {
                     self.pageControl.currentPage = visibleIndexPath.row
            }
        }
        
       
    }
    
    
}
