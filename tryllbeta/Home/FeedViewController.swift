//
//  FeedViewController.swift
//  tryllbeta
//
//  Created by Tryll on 7/2/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class FeedViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let dispatchGroup = DispatchGroup()
    var postsIds: [String] = []
    var suitcasedTrylls: [Bool] = []
    var feedArray : [[String:Any]] = []
    var selectedIndexPath = IndexPath()
    var uidTemp = ""
    func loadFromFirebase()  {
        //        dispatchGroup.enter()
        allFollowingPosts.removeAll()
        suitcasedTrylls.removeAll()
        postsIds.removeAll()
        feedArray.removeAll()
        DispatchQueue.main.async {
            self.feedCollectionView.reloadData()
        }
        
        
        guard let uid = Auth.auth().currentUser?.uid else{
            self.showAlert(title: "Error happened", message: "Failed at downloading data for exlpore, Reason: You are not logged in,", completion: nil)
            return
        }
       Firestore.firestore().collection("Users").document(uid).collection("Following").getDocuments { (snapshot, err) in
            // Check for no error
            self.uidTemp = uid
            guard err == nil else{return}
            //get every person I follow
            snapshot?.documents.forEach({ (f) in
                let followedRef = f.data() as! [String:String]
                // Get every post by each
                Firestore.firestore().collection("Trylls").whereField("created_by_uid", isEqualTo: followedRef["user_Id"]!).getDocuments(completion: { (snapshot2, err) in
                    snapshot2?.documents.forEach({
                        let post = $0.data()
                        print("total data \(post)")
                        
                        
                        if let totallike = post["likes"] as? [String]{
                            print("total likes \(totallike.count)")
                        }
                        
                        let isSuitcaseSelected = post["isSuitcaseSelected"] as? Int ?? 0
                        if  isSuitcaseSelected == 1 {
                            self.suitcasedTrylls.append(true)
                        } else {
                            self.suitcasedTrylls.append(false)
                        }
                        
                        let documentId = $0.documentID
                        self.postsIds.append(documentId)
                        allFollowingPosts.append(post)
                        self.feedArray.append(post)
                        
                        //allFollowingPosts = allFollowingPosts.sorted(by: { ($0["timestamp"] as? Timestamp)!.seconds > ($1["timestamp"] as? Timestamp)!.seconds })
                        //self.feedArray = self.feedArray.sorted(by: { ($0["timestamp"] as? Timestamp)!.seconds > ($1["timestamp"] as? Timestamp)!.seconds })
                        
                        DispatchQueue.main.async {
                            self.feedCollectionView.reloadData()
                            if self.selectedIndexPath.count != 0 {
                                self.feedCollectionView.scrollToItem(at: self.selectedIndexPath, at: [.centeredVertically, .centeredHorizontally], animated: false)
                            }
                        }
                    })
                })
            })
        }
    }
    
    @IBOutlet weak var feedCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "ProximaNova-Semibold", size: 20)!]
        self.feedCollectionView.backgroundColor = UIColor.tryllGrey
        feedCollectionView.delegate = self
        feedCollectionView.dataSource = self
        FollowSuggestionViewController.followIndex = 0
        //loadFromFirebase()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.feedCollectionView.backgroundColor = UIColor.tryllGrey
//        feedCollectionView.delegate = self
//        feedCollectionView.dataSource = self
//        FollowSuggestionViewController.followIndex = 0
        loadFromFirebase()
        
    }
    
    @objc func TotallikeClicked(sender:UIButton)
    {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FollowSuggestionViewController") as! FollowSuggestionViewController
        vc.fromFeeds = true
        
        if let likes = feedArray[sender.tag]["likes"] as? [String] {
            vc.likeUser  = likes
        } else {
            vc.likeUser = []
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func messageAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Message", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MessageFriendListView") as! MessageFriendListView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    //to display the number of collection cells/ posts in the feed
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if feedArray.count != 0 {
            return feedArray.count
        }
        return allFollowingPosts.count
    }
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeedsCollectionViewCell
            if feedArray.count != 0 {
                DispatchQueue.main.async {
                    print("from feed array")
                    let item = self.feedArray[indexPath.row]
                    cell.delegate = self
                    cell.createdByUID = item["created_by_uid"] as? String ?? ""
                    cell.postId = self.postsIds[indexPath.row]
                    
                    let imageArray = item["images"] as! [String]
                    let firstImage =  imageArray[0] as String
                    
                    let imgRef = Storage.storage().reference().child(firstImage)
                    cell.postedImage.sd_setImage(with: imgRef, placeholderImage: #imageLiteral(resourceName: "Group"))
                    //cell.postedImage.contentMode = .scaleAspectFit
                    if self.suitcasedTrylls[indexPath.row] {
                        cell.suitcaseButton.setImage(UIImage(named: "suitcase_selected"), for: .normal)
                    } else {
                        cell.suitcaseButton.setImage(UIImage(named: "suitcase_unselected"), for: .normal)
                    }
                    
                 cell.likeButton.tag = indexPath.row
                    if let totallike = item["likes"] as? [String]{
                        print("total likes \(totallike.count)")
                        cell.likeCount = totallike.count
                        
                        if totallike.count > 0 {
                            cell.totalLikeCountBtn.setTitle(String(totallike.count), for: .normal)
                            if totallike.contains(Auth.auth().currentUser?.uid ?? "") {
                                print("post id \(self.postsIds[indexPath.row])")
                                print("indexpath \(indexPath.row) like section")
                                cell.islikeButtonOn = true
                                cell.likeButton.setImage(UIImage(named:"like"), for: .normal)
                                //cell.likeButton.addTarget(self,action:#selector(self.unlikeClicked), for:.touchUpInside)
                                
                            } else {
                                print("indexpath \(indexPath.row) unlike section")
                                cell.islikeButtonOn = false
                                cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                                //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                            }
                        } else {
                            cell.likeCount = 0
                            cell.islikeButtonOn = false
                            cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                            cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                        }
                        
                        
                                       
                    } else {
                        cell.likeCount = 0
                        cell.islikeButtonOn = false
                        cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                        cell.totalLikeCountBtn.setTitle("", for: .normal)
                        //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                    }
                    cell.totalLikeCountBtn.tag = indexPath.row
                    
                   cell.totalLikeCountBtn.addTarget(self,action:#selector(self.TotallikeClicked), for:.touchUpInside)
                    
                    if let totalComments = item["comments"] as? [[String : Any]] {
                        cell.totalCommentsBtn.setTitle(String(totalComments.count), for: .normal)
                    } else {
                        cell.totalCommentsBtn.setTitle("", for: .normal)
                    }
                    
                    
                    let profileImgRef = Storage.storage().reference().child("user").child(item["created_by_uid"] as! String).child("profile_photo.jpg")
                    cell.profileImage.sd_setImage(with: profileImgRef)
                    //cell.timeStamp.text = item["timestamp"] as? String ?? ""
                    cell.username.text = item["username"] as? String
                    let location =  item["location"] as! [String:String]
                    cell.cityLabel.text = location["address"]
                    cell.postedImageLocation.text = item["place_name"] as? String
                    cell.postedImageDescription.text = item["headline"] as? String
                    cell.postedImageDescription.numberOfLines=0
                    cell.postedImageDescription.lineBreakMode = .byCharWrapping
                    cell.postedImageDescription.sizeToFit()
                    let postTimeStr = item["timestamp"] as? String
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
                    let postTime = formatter.date(from: postTimeStr ?? "")
                    let time = postTime?.timeAgoDisplay()
                    print("timeAgo ... \(time ?? "NA")")
                    cell.timeStamp.text = time//item["timestamp"] as? String
                    cell.timeStamp.isHidden = false
    //                let timestamp = item["timestamp"] as? Timestamp
    //                var timeStamp2 = Double(timestamp!.nanoseconds)
    //                timeStamp2 = timeStamp2 / 1000000
    //                let converted = NSDate(timeIntervalSince1970: timeStamp2 / 1000)
    //
    //
    //                let dateFormatter = DateFormatter()
    //                dateFormatter.timeZone = NSTimeZone.local
    //                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
    //                let time = dateFormatter.string(from: converted as Date)
                    
    //                let dateTime = timeStamp2.convertToDate(timeStamp: timeStamp2, fordateFormat: "dd/MM/yyyy hh:mm")
                    cell.postedImageReaction.image = updateEmojiRatingCatagory(item["emojiRating"] as! Int)
                    let emojiRating = item["emojiRating"] as! Int
                    
    //                switch emojiRating {
    //                case 4:
    //                    cell.postedImageDescription.textColor=UIColor(red: 172.0/255, green: 8.0/255, blue: 0, alpha: 1.0)
    //                case 5:
    //                    cell.postedImageDescription.textColor=UIColor(red: 250.0/255, green: 78.0/255, blue: 68.0/255, alpha: 1.0)
    //                case 6:
    //                    cell.postedImageDescription.textColor=UIColor(red: 250.0/255, green: 107.0/255, blue: 1.0/255, alpha: 1.0)
    //                case 7:
    //                    cell.postedImageDescription.textColor=UIColor(red: 252.0/255, green: 168.0/255, blue: 3.0/255, alpha: 1.0)
    //                case 8:
    //                    cell.postedImageDescription.textColor=UIColor(red: 253.0/255, green: 203.0/255, blue: 40.0/255, alpha: 1.0)
    //                default:
    //                    cell.postedImageDescription.textColor=UIColor(red: 253.0/255, green: 203.0/255, blue: 40.0/255, alpha: 1.0)
    //                }
                    
                    if let tags = item["tags"] as? [String]{
                        cell.postedImageCategory.image = updateImageCatagory(tags)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    print("from feed following post")
                    let item = allFollowingPosts[indexPath.row]
                    cell.delegate = self
                    cell.createdByUID = item["created_by_uid"] as? String ?? ""
                    cell.postId = self.postsIds[indexPath.row]
                    let imgRef = Storage.storage().reference().child(item["images"] as! String)
                    cell.postedImage.sd_setImage(with: imgRef, placeholderImage: #imageLiteral(resourceName: "Group"))
                   // cell.postedImage.contentMode = .scaleAspectFit
                    if self.suitcasedTrylls[indexPath.row] {
                        cell.suitcaseButton.setImage(UIImage(named: "suitcase_selected"), for: .normal)
                    } else {
                        cell.suitcaseButton.setImage(UIImage(named: "suitcase_unselected"), for: .normal)
                    }
                    
                    let profileImgRef = Storage.storage().reference().child("user").child(item["created_by_uid"] as! String).child("profile_photo.jpg")
                    cell.profileImage.sd_setImage(with: profileImgRef)
                    cell.timeStamp.text = item["timestamp"] as? String ?? ""
                    cell.username.text = item["username"] as? String
                    let location =  item["location"] as! [String:String]
                    cell.cityLabel.text = location["address"]
                    cell.postedImageLocation.text = item["place_name"] as? String
                    cell.postedImageDescription.text = item["headline"] as? String
                    cell.postedImageDescription.numberOfLines=0
                    cell.postedImageDescription.lineBreakMode = .byCharWrapping
                    cell.postedImageDescription.sizeToFit()
                    //cell.timeStamp.text =   item["timestamp"] as? String
                    
                    let postTimeStr = item["timestamp"] as? String
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
                    let postTime = formatter.date(from: postTimeStr ?? "")
                    let time = postTime?.timeAgoDisplay()
                    print("timeAgo ... \(time ?? "NA")")
                    cell.timeStamp.text = time//item["timestamp"] as? String
                    cell.timeStamp.isHidden = false
                    
                    if let totallike = item["likes"] as? [String]{
                        print("total likes \(totallike.count)")
                        cell.likeCount = totallike.count
                        cell.totalLikeCountBtn.setTitle(String(totallike.count), for: .normal)
                        
                        if totallike.count > 0 {
                            if totallike.contains(self.uidTemp) {
                                print("post id \(self.postsIds[indexPath.row])")
                                print("indexpath \(indexPath.row) like section")
                                cell.islikeButtonOn = true
                                cell.likeButton.setImage(UIImage(named:"like"), for: .normal)
                                //cell.likeButton.addTarget(self,action:#selector(self.unlikeClicked), for:.touchUpInside)
                                
                            } else {
                                print("indexpath \(indexPath.row) unlike section")
                                cell.islikeButtonOn = false
                                cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                                //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                            }
                        } else {
                            cell.likeCount = 0
                            cell.islikeButtonOn = false
                            cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                            cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                        }
                        
                        
                                       
                    } else {
                            cell.likeCount = 0
                            cell.islikeButtonOn = false
                            cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                            cell.totalLikeCountBtn.setTitle(String("0"), for: .normal)
                       // cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                    }
                    
                    
                    cell.likeButton.tag = indexPath.row
                    
                    
                    
                    
                    cell.postedImageReaction.image = updateEmojiRatingCatagory(item["emojiRating"] as! Int)
                    let emojiRating = item["emojiRating"] as! Int
                    
                    cell.totalLikeCountBtn.addTarget(self,action:#selector(self.TotallikeClicked), for:.touchUpInside)
                    
                    if let totalComments = item["comments"] as? [[String : Any]] {
                        cell.totalCommentsBtn.setTitle(String(totalComments.count), for: .normal)
                    } else {
                        cell.totalCommentsBtn.setTitle("", for: .normal)
                    }
                    
                    
    //                switch emojiRating {
    //                case 4:
    //                    cell.postedImageDescription.textColor=UIColor(red: 172.0/255, green: 8.0/255, blue: 0, alpha: 1.0)
    //                case 5:
    //                    cell.postedImageDescription.textColor=UIColor(red: 250.0/255, green: 78.0/255, blue: 68.0/255, alpha: 1.0)
    //                case 6:
    //                    cell.postedImageDescription.textColor=UIColor(red: 250.0/255, green: 107.0/255, blue: 1.0/255, alpha: 1.0)
    //                case 7:
    //                    cell.postedImageDescription.textColor=UIColor(red: 252.0/255, green: 168.0/255, blue: 3.0/255, alpha: 1.0)
    //                case 8:
    //                    cell.postedImageDescription.textColor=UIColor(red: 253.0/255, green: 203.0/255, blue: 40.0/255, alpha: 1.0)
    //                default:
    //                    cell.postedImageDescription.textColor=UIColor(red: 253.0/255, green: 203.0/255, blue: 40.0/255, alpha: 1.0)
    //                }
                    
                    if let tags = item["tags"] as? [String]{
                        cell.postedImageCategory.image = updateImageCatagory(tags)
                    }
                }
            }
            
            return cell
        }
    
    // unwinds to feed view controller on clicking back button of fullImage View controller
    @IBAction func UnwindToFeedView(unwindSegue: UIStoryboardSegue){
        print("Pressed Back button")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 510)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        let vc = storyboard?.instantiateViewController(withIdentifier: "FullImageViewController") as! FullImageViewController
        self.navigationController?.navigationBar.tintColor = .tryllBlack
        if feedArray.count != 0 {
            let item = feedArray[indexPath.row]
            let itemId = postsIds[indexPath.row]
            
            //let imgRef = Storage.storage().reference().child(item["images"] as! String)
            //        vc.post = imgRef
            vc.postData = item
            vc.postId = itemId
        }else{
            let item = allFollowingPosts[indexPath.row]
            let itemId = postsIds[indexPath.row]
            
            //let imgRef = Storage.storage().reference().child(item["images"] as! String)
            //        vc.post = imgRef
            vc.postData = item
            vc.postId = itemId
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension FeedViewController: FeedsCollectionViewCellDelegate {
    func openProfile(createdByUID: String) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.userId = createdByUID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension String {
    func convertToDate(timeStamp:String, fordateFormat dateFormat: String) -> String {
        let time_stamp = Double(timeStamp)! / 1000
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .full
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = dateFormat
        
        let dateFormatter4hour = DateFormatter()
        dateFormatter4hour.timeStyle = .none
        dateFormatter4hour.dateStyle = .full
        dateFormatter4hour.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter4hour.dateFormat = "HH"
        
        let dateFormatter4minute = DateFormatter()
        dateFormatter4minute.timeStyle = .none
        dateFormatter4minute.dateStyle = .full
        dateFormatter4minute.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter4minute.dateFormat = "mm"
        
        var dates = Date(timeIntervalSince1970: time_stamp)
        
        let strhour = dateFormatter4hour.string(from: dates)
        let strminute = dateFormatter4minute.string(from: dates)
        
        if strhour == "00" && strminute == "00" {
            let secondsInTwelveHours = TimeInterval(12 * 60 * 60)
            dates = dates.addingTimeInterval(secondsInTwelveHours)
        }
        
        let getDate = dateFormatter.string(from: dates)
        
        return getDate
    }
}


extension Date {
    func timeAgoDisplay() -> String {

        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!

        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "\(diff) sec ago"
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "\(diff) min ago"
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "\(diff) hrs ago"
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return "\(diff) days ago"
        }
        let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
        return "\(diff) weeks ago"
    }
}
