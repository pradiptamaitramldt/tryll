//
//  ForgotPasswordViewController.swift
//  tryllbeta
//
//  Created by Chintan Sanjay Puri on 2/27/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func resetPassword(_ sender: UIButton) {
        guard let emailText = emailTextField.text, emailText != "" else {
            showAlert(title: "Invalid Email", message: "Please enter correct email address", completion: nil)
            return
        }
        Auth.auth().sendPasswordReset(withEmail: emailText) { (error) in
            if error == nil {
                self.showAlert(title: "Email Sent", message: "A password reset email has been successfully sent") {
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Error", message: "Unable to send email. Please try again later", completion: nil)
            }
        }
    }
}
