//
//  CustomCreateTagView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 03/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit

protocol CustomTagDelegate : NSObjectProtocol {
    func doneCustomTag(tag: Int)
}

class CustomCreateTagView: UIView {

    let nibName = "CustomCreateTagView"
    var contentView: UIView?
    weak var delegate : CustomTagDelegate?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        instanceFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instanceFromNib()
    }
    
    
    private func instanceFromNib() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(viewContainer)
        viewContainer.frame = self.bounds
        viewContainer.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var textFieldTag: UITextField!
    @IBOutlet weak var buttonDone: UIButton!
    
    
    @IBAction func buttonDoneAction(_ sender: UIButton) {
        self.delegate?.doneCustomTag(tag: 0)
    }
}
