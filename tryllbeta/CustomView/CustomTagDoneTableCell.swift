//
//  CustomTagDoneTableCell.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 03/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit

class CustomTagDoneTableCell: UITableViewCell {

    @IBOutlet weak var textFieldTag: UITextField!
    @IBOutlet weak var buttonDone: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
