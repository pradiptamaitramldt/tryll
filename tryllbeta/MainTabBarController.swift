//
//  MainTabBarController.swift
//  tryllbeta
//
//  Created by Surabhi chavan on 06/08/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos

class MainTabBarController: UITabBarController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var isShareSelected : Bool = true
    
    let button = UIButton.init(type: .custom)
    let cameraButton = UIButton.init(type: .custom)
    let galleryButton = UIButton.init(type: .custom)
    var closeButton = UIButton.init(type: .custom)
    let bgView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*if #available(iOS 13.0, *) {
            let appearance = tabBar.standardAppearance
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
            tabBar.backgroundColor = UIColor.white
        } else {
            // Fallback on earlier versions
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
            tabBar.backgroundColor = UIColor.white
        }*/
//        tabBar.shadowColor = nil
//        tabBar.shadowImage = nil
//        tabBar.backgroundImage = UIImage()
//        tabBar.backgroundColor = UIColor.white
        
        
        button.frame = CGRect(x: 10  , y: 0, width: 44, height: 44)
        
        self.view.insertSubview(button, aboveSubview: self.tabBar)
        button.setImage(UIImage(named: "shareIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.bottomAnchor.constraint(equalTo: tabBar.bottomAnchor, constant: 2).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
        button.widthAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        let window = UIApplication.shared.keyWindow
        if let bottomPadding = window?.safeAreaInsets.bottom{
            print("bottomPadding")
            print(bottomPadding)
            
            button.addTarget(self, action: #selector(showShareOptions), for: .touchUpInside)
        }else{
            
            button.frame = CGRect(x: 10 , y: 0, width: 44, height: 44)
            button.setImage(UIImage(named: "shareIcon"), for: .normal)
            button.addTarget(self, action: #selector(showShareOptions), for: .touchUpInside)
            self.view.insertSubview(button, aboveSubview: self.tabBar)
        }
        
        
        bgView.frame=CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        bgView.backgroundColor=UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        bgView.isHidden=true
        self.view.addSubview(bgView)
        
        cameraButton.frame = CGRect(x: 100, y: 0, width: 40, height: 40)
        cameraButton.layer.cornerRadius = 20
        cameraButton.setImage(UIImage(named: "Camera Icon white"), for: .normal)
        cameraButton.isHidden = true
        cameraButton.addTarget(self, action: #selector(showCamera), for: .touchUpInside)
        
        self.view.insertSubview(cameraButton, aboveSubview: self.tabBar)
        self.view.bringSubviewToFront(cameraButton)
        
        galleryButton.frame = CGRect(x: 100, y: 0, width: 40, height: 40)
        galleryButton.setImage(UIImage(named: "Gallery Icon white"), for: .normal)
        
        galleryButton.layer.cornerRadius = 20
        
        galleryButton.isHidden = true
        galleryButton.addTarget(self, action: #selector(showGallery), for: .touchUpInside)
        
        self.view.insertSubview(galleryButton, aboveSubview: self.tabBar)
        self.view.bringSubviewToFront(galleryButton)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let window = UIApplication.shared.keyWindow
        
        if let bottomSafeAreaPadding = window?.safeAreaInsets.bottom{
            
            // Change these value to configure the icon
            //let tryllIconInsets : CGFloat = 6
            //let tabheight = self.tabBar.bounds.height - tryllIconInsets
            //let raiseBy : CGFloat = 8
            //let iconRadius : CGFloat = tabheight + raiseBy - bottomSafeAreaPadding
            //let iconYAxisPlace : CGFloat = iconRadius + bottomSafeAreaPadding + tryllIconInsets/2
            
            
            //button.frame = CGRect.init(x: self.tabBar.center.x - (iconRadius/2) , y: self.view.bounds.height - iconYAxisPlace , width: iconRadius, height: iconRadius)
            button.frame = CGRect.init(x: self.tabBar.center.x - 32, y: self.view.bounds.height - 74, width: 64, height: 64)
            button.layer.cornerRadius = 32 //iconRadius/2
            button.layer.borderWidth = 10.0
            //button.layer.borderColor = UIColor(red: 247.0/255, green: 246.0/255, blue: 246.0/255, alpha: 1.0).cgColor
            button.layer.borderColor = UIColor.white.cgColor
            print("bottomPadding")
            print(bottomSafeAreaPadding)
        }else{
            button.frame = CGRect(x: 10 , y: 44, width: 44, height: 44)
            button.setImage(UIImage(named: "shareIcon"), for: .normal)
        }
        
        cameraButton.frame = CGRect.init(x: self.tabBar.center.x - 50, y: self.view.bounds.height - 130, width: 40, height: 40)
        
        galleryButton.frame = CGRect.init(x: self.tabBar.center.x + 20, y: self.view.bounds.height - 130, width: 40, height: 40)
        self.bgView.sendSubviewToBack(bgView)
        tabBar.backgroundColor = UIColor.white
    }
    
    @objc func showShareOptions() {
        if isShareSelected {
            closeButton = UIButton.init(frame: CGRect(x: self.tabBar.center.x - 32, y: self.view.bounds.height - 74, width: 64, height: 64))
            closeButton.layer.cornerRadius = 32 //iconRadius/2
            //closeButton.layer.borderWidth = 10.0
            closeButton.backgroundColor=UIColor.white
//            closeButton.layer.borderColor = UIColor.white.cgColor
            closeButton.setImage(UIImage(named: "close_yellow"), for: .normal)
            closeButton.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
            self.view.insertSubview(closeButton, aboveSubview: self.tabBar)
            self.view.bringSubviewToFront(closeButton)
            
            cameraButton.isHidden = false
            galleryButton.isHidden = false
            bgView.isHidden = false
            isShareSelected = false
            
        } else {
            closeButton.removeFromSuperview()
            cameraButton.isHidden = true
            galleryButton.isHidden = true
            bgView.isHidden = true
            isShareSelected = true
        }
    }
    
    @objc func closePopup() {
        cameraButton.isHidden = true
        galleryButton.isHidden = true
        bgView.isHidden = true
        isShareSelected = true
        closeButton.removeFromSuperview()
    }
    
    @objc func showGallery() {
        /*let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.modalPresentationStyle = .fullScreen
        
        self.present(imagePickerController, animated: true, completion: nil)*/
        
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 3
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func showCamera() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        imagePickerController.sourceType = .camera
        
        imagePickerController.modalPresentationStyle = .fullScreen
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    //After you are done selecting the image either from camera or photolibrary, this function gets called
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imagePicked = info[UIImagePickerController.InfoKey.originalImage] as? UIImage

        picker.dismiss(animated: true, completion: nil)
        cameraButton.isHidden = true
        galleryButton.isHidden = true
        isShareSelected = true
        bgView.isHidden = true
        closeButton.removeFromSuperview()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "photoLibrary") as! PhotoLibraryDetailController
        controller.isFromPhotoLibrary = false
        controller.pickedImage = imagePicked
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: {
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        cameraButton.isHidden = true
        galleryButton.isHidden = true
        isShareSelected = true
        bgView.isHidden = true
        closeButton.removeFromSuperview()
    }
}
extension MainTabBarController: OpalImagePickerControllerDelegate {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        //Cancel action?
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        //Save Images, update UI
        
        //Dismiss Controller
        presentedViewController?.dismiss(animated: true, completion: nil)
        
        cameraButton.isHidden = true
        galleryButton.isHidden = true
        isShareSelected = true
        bgView.isHidden = true
        closeButton.removeFromSuperview()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "photoLibrary") as! PhotoLibraryDetailController
        controller.isFromPhotoLibrary = true
        controller.selectedImages = assets
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: {
            controller.selectedImages = assets
        })
    }
    
//    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
//        return 0
//    }
//
//    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
//        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
//    }
//
//    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
//        return URL(string: "https://placeimg.com/500/500/nature")
//    }
}

