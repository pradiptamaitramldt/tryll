//
//  CommentTableViewCell.swift
//  tryllbeta
//
//  Created by Surabhi chavan on 14/11/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    @IBOutlet var userProfileImageView: UIImageView!
    
    @IBOutlet var userName: UILabel!
    
    @IBOutlet var commentLabel: UILabel!
    
    @IBOutlet var timeStampLabel: UILabel!
    
override func awakeFromNib() {
   super.awakeFromNib()

    }
}
