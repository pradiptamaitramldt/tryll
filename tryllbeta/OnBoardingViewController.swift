//
//  ViewController.swift
//  onBoarding
//
//  Created by Muhannad Alnemer on 8/1/19.
//  Copyright © 2019 Muhannad Alnemer. All rights reserved.
//

import UIKit
import paper_onboarding
import SAConfettiView

class onBoardingViewController: UIViewController, PaperOnboardingDataSource,PaperOnboardingDelegate {
    @IBAction func skipButtonClicked(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "skippedOnboarding")
    }
    
    @IBOutlet weak var skipButton: UIButton!
    var confettiView: SAConfettiView!
   
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            
            let defualts = UserDefaults.standard
            if defualts.bool(forKey: "skippedOnboarding"){
                self.performSegue(withIdentifier: "segueToLogin", sender: self)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        
//        print("skippppp")
//        print(defualts.bool(forKey: "skippedOnboarding"))
        
        
        
        confettiView = SAConfettiView(frame: self.view.bounds)
        
        let onboarding = PaperOnboarding()
        onboarding.dataSource = self
        onboarding.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(onboarding)
        confettiView.type = .Image(UIImage(named: "white1")!)
        confettiView.colors = [UIColor.tryllWhite, UIColor.tryllYellow, UIColor.black]
        self.view.addSubview(confettiView)
        
        
        
        confettiView.startConfetti()
        
        
        
        
        confettiView.isUserInteractionEnabled = false
        // add constraints
        for attribute: NSLayoutConstraint.Attribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboarding,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
        
        
        view.bringSubviewToFront(skipButton)
        
    }
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        let titleFontSize = CGFloat(25)
        let descriptionFontSize = CGFloat(13)
        
        return [
            OnboardingItemInfo(informationImage: UIImage(named: "Group 6")!,
                               title: "Welcome to Tryll",
                               description: "",
                               pageIcon: UIImage(named: "Group 6")!,
                               color: UIColor.tryllGrey,
                               titleColor: UIColor.black,
                               descriptionColor: UIColor.gray,
                               titleFont: UIFont.boldSystemFont(ofSize: titleFontSize),
                               descriptionFont: UIFont.systemFont(ofSize: descriptionFontSize)),
            
            OnboardingItemInfo(informationImage: UIImage(named: "logo")!,
                               title: "Tryll it!",
                               description: "Tryll a place to share with your friend and followers",
                               pageIcon: UIImage(named: "logo")!,
                               color: UIColor.tryllYellow,
                               titleColor: UIColor.white,
                               descriptionColor: UIColor.gray,
                               titleFont: UIFont.boldSystemFont(ofSize: titleFontSize),
                               descriptionFont: UIFont.systemFont(ofSize: descriptionFontSize)),
            
            OnboardingItemInfo(informationImage: UIImage(named: "work_outline_24px_outlined")!,
                               title: "Suitcase it!",
                               description: "Suitcasing a tryll will allow you to visit it later and share your experience",
                               pageIcon: UIImage(named: "work_outline_24px_outlined")!,
                               color: UIColor.tryllYellow,
                               titleColor: UIColor.white,
                               descriptionColor: UIColor.gray,
                               titleFont: UIFont.boldSystemFont(ofSize: titleFontSize),
                               descriptionFont: UIFont.systemFont(ofSize: descriptionFontSize))
            ][index]
    }
    
    func onboardingItemsCount() -> Int {
        return 3
    }
}
