//
//  SignUpViewController.swift
//  tryllbeta
//
//  Created by Tryll on 6/27/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

import FirebaseAuth
import FirebaseDatabase
import FirebaseFirestore
import Firebase
import Foundation
import FirebaseStorage

class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var usernameField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var createButton: UIButton!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var addImageButton: UIButton!
    
    
    
    var imagePicker:UIImagePickerController!
    
    let userDefault = UserDefaults.standard
    
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        usernameField.becomeFirstResponder()
        
        
        createButton.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
        
        addImageButton.addTarget(self, action: #selector(openImagePicker), for: .touchUpInside)
        addImageButton.layer.cornerRadius = addImageButton.bounds.width/2
        
        
        imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        profileImageView.layer.cornerRadius = profileImageView.bounds.height / 2
        profileImageView.clipsToBounds = true 
    
    }
    
    
    
    @objc func openImagePicker(_ sender:Any){
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Resigns the target textField and assigns the next textField in the form.
        switch textField {
        case usernameField:
            usernameField.resignFirstResponder()
            emailField.becomeFirstResponder()
            break
        case emailField:
            emailField.resignFirstResponder()
            passwordField.becomeFirstResponder()
            break
        case passwordField:
            handleSignUp()
            break
        default:
            break
        }
        return true
    }
    
    
    
    
    @objc func handleSignUp() {
        guard let username = usernameField.text else {return}
        guard let email = emailField.text else {return}
        guard let pass = passwordField.text else {return}
        guard let image = profileImageView.image else {return}
        
      
        
        
        self.showSpinner(onView: self.view)
        Auth.auth().createUser(withEmail: email, password: pass) { user, error in
            
            guard error == nil else {
                self.removeSpinner()
                self.showAlert(title: "Error", message: "\(error!.localizedDescription)", completion: nil)
                return
            }
            
            guard user != nil else {
                self.removeSpinner()
                
                self.showAlert(title: "Error", message: "\(error!.localizedDescription)", completion: nil)
                return
            }

            self.createButton.isUserInteractionEnabled = false
            
            // 1. Upload the profile image to firebase storage
            uploadProfileImage(image) { url in
                if url != nil {
                    
                    // 2. Save the profile data to firebase database
                    saveProfile(username: username, withEmail: email, profileImageURL: url!) { success in
                        if success {
                            self.removeSpinner()
                            self.performSegue(withIdentifier: "toLogIn", sender: self)
                        }
                    }
                    
                    
                }else{
                    self.createButton.isUserInteractionEnabled = true
                    self.showAlert(title: "Error", message: "There seem to be an issue with registering your email, try another one", completion: nil)
                }
            }
            
        }
            
        
        
        func uploadProfileImage(_ image: UIImage, completion: @escaping ((_ url:URL?)->())){
            guard let uid = Auth.auth().currentUser?.uid else { return }
            let storageRef = Storage.storage().reference().child("user/\(uid)/profile_photo.jpg")
            
            guard let imageData = image.jpegData(compressionQuality: 0.75) else { return }
            
            let metaData = StorageMetadata()
            metaData.contentType = "image/JPEG"
            
            storageRef.putData(imageData, metadata: metaData) { metaData, error in
                if error == nil, metaData != nil {
                    storageRef.downloadURL() { url, error in
                        completion(url)
                    }
                } else {
                    
                    //failed!
                    completion(nil)
                }
            }
            
        }
        
        func saveProfile(username:String, withEmail: String, profileImageURL: URL, completion: @escaping ((_ success: Bool)->())) {
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            let userObject : [String:Any] = [
                "uid":uid,
                "username": username,
                "profile_picture": "user/\(uid)/profile_photo.jpg",
                "first_name" : "",
                "last_name" : "",
                "phone" : "",
                "email" : email,
                "bio" : "",
                "profileType": "Public"
                ]
            
            Firestore.firestore().collection("Users").document(uid).setData(userObject)
            completion(true)
        }
        
        
        func textFieldChanged(_ target:UITextField) {
            let username = usernameField.text
            let email = emailField.text
            let password = passwordField.text
            let formFilled = username != nil && username != "" && email != nil && email != "" && password != nil && password != ""
         
            setcreateButton(enabled: true)
        }
        
        func setcreateButton(enabled:Bool) {
            if enabled {
                
                createButton.isEnabled = true
            } else {
                
                createButton.isEnabled = false
            }
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
}

extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
  
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let pickedImage = info[.editedImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        self.profileImageView.image = pickedImage
        
        
        picker.dismiss(animated: true, completion: nil)
    }
}
