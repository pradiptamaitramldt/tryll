//
//  SlideViewController.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 7/30/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import PanModal
import FirebaseFirestore
import FirebaseUI

class SlideViewController: UIViewController {
    
    
    public var selectedCountry = ""
    public var personArray :[Person] = []
    public var resturantArray :[Resturant] = []
    public var filteredResturantArray :[Resturant] = []
    var postData = [[String:Any]]()
    var postId = [String]()

    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countLabel: UILabel!
    
    
    @IBAction func countryButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:  View controller life cycle

    
    override func viewWillAppear(_ animated: Bool) {

    }
    override func viewWillDisappear(_ animated: Bool) {
        personArray.removeAll()
        resturantArray.removeAll()
        filteredResturantArray.removeAll()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countLabel.text = "\(resturantArray.count)"
        countryButton.setTitle(selectedCountry, for: .normal)
        countryButton.titleLabel?.adjustsFontSizeToFitWidth = true
    filteredResturantArray = resturantArray

    personArray = personArray.unique{ $0.uid }
    
    collectionView.reloadData()
    }
}


// MARK: - TableViewDelegates


extension SlideViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredResturantArray.count
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ResturantSuggestionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "suggestionCell") as! ResturantSuggestionTableViewCell
        
        cell.addressLabel.text = filteredResturantArray[indexPath.row].address
        cell.resturantLabel.text = filteredResturantArray[indexPath.row].name
        let imgRef = Storage.storage().reference().child(filteredResturantArray[indexPath.row].imageRef!)
        print(imgRef.fullPath)
        cell.backgroundImage.sd_setImage(with: imgRef)
        
        cell.selectionStyle = .none
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let vc = storyboard?.instantiateViewController(withIdentifier: "FullImageViewController") as! FullImageViewController
            self.navigationController?.navigationBar.tintColor = .tryllBlack
        vc.postData=self.postData[indexPath.row]
        vc.postId=self.postId[indexPath.row]
            present(vc, animated: false)
        
    }
}


// MARK: - CollectionViewDelegate

extension SlideViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return personArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:PersonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "personCell", for: indexPath) as! PersonCollectionViewCell
        
        let person = personArray[indexPath.row]
        
        cell.nameLabel.text = person.name
        cell.avatarImageView.sd_setImage(with: Storage.storage().reference(withPath: "user/\(person.uid)/profile_photo.jpg"), placeholderImage: #imageLiteral(resourceName: "shareIcon"))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let indexPath = collectionView.indexPathsForSelectedItems?.first
        let currentCell = collectionView.cellForItem(at: indexPath) as! PersonCollectionViewCell
        
//        collectionView.cell
        currentCell.nameLabel.textColor = .tryllYellow
        
        filteredResturantArray = resturantArray.filter({$0.postedBy?.uid == personArray[indexPath.row].uid})
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.userId = personArray[indexPath.row].uid
        present(vc, animated: false)
        
        self.tableView.reloadData()
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cellToDeselect = collectionView.cellForItem(at: indexPath) as! PersonCollectionViewCell
        
        cellToDeselect.nameLabel.textColor = .tryllBlack
    }
}

// MARK: - Pan Modal Delegates



extension SlideViewController:PanModalPresentable{
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var panScrollable: UIScrollView? {
        return nil
    }

    var longFormHeight: PanModalHeight {
        return .maxHeightWithTopInset(50)
    }
    var shortFormHeight: PanModalHeight {
        if isViewLoaded {
            return .contentHeight(240)
        }
        return .maxHeight
    }
}


extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}
