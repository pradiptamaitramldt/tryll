//
//  ExploreViewController.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 7/28/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import Mapbox
import MapboxGeocoder
import PanModal
import Alamofire
import Alamofire_SwiftyJSON

var allFollowingPosts : [[String:Any]] = []

class ExploreViewController: UIViewController {
    
    var layerIndex : Int = 0
    var countriesArray :[String] = []
    var filteredPost :[[String:Any]] = []
    var filters : [String] = []
//    weak var delegate: DetailViewControllerDelegate?
    var filterViewController: FilterViewController!
    
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var mapView: MGLMapView!
    @IBOutlet weak var searchTextFeild: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    
    @IBAction func filterButtonClicked(_ sender: Any) {
//        self.performSegue(withIdentifier: "FilterView", sender: self)
        let vc = UINavigationController(rootViewController: filterViewController)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func search(for country: String) {
        let geocode = Geocoder.shared
        print(country)
        print(country.capitalized)
        
        let option = ForwardGeocodeOptions(query: country)
        option.allowedScopes = [.country]
        geocode.geocode(option) { (place, _, _) in
            guard let location = place?.first?.location?.coordinate else{
                self.showAlert(title: "Error", message: "Couldn't find the \(country)", completion: nil)
                return
            }
            guard let name = place?.first?.name else {return}
            
            //            self.mapView.setCamera(a, animated: true)
            self.mapView.setCenter(location, animated: true)
            self.openSlide(for: name)
        }
    }
    
    
    func search(for coordiantes: CLLocationCoordinate2D) {
        let geocode = Geocoder.shared
//        print(country)
//        print(country.capitalized)
//        let option = ForwardGeocodeOptions(query: country)
        let option = ReverseGeocodeOptions(coordinate: coordiantes)
        option.allowedScopes = [.country]
        geocode.geocode(option) { (place, _, _) in
            guard let location = place?.first?.location?.coordinate else{return}
//            self.mapView.setCamera(a, animated: true)
            self.mapView.setCenter(location, animated: true)
        }
    }

    @objc func openSlide(){
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "SlideView")) as! SlideViewController

//        vc.selectedCountry = "India"
//        presentPanModal(vc)
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    @objc func openSlide(for country:String) {
        
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "SlideView")) as! SlideViewController
        
        // Filter the posts for the $country
        let posts = filteredPost.filter({
            let post = $0["location"] as! [String:Any]
            return country == (post["country"] as! String)
        })
        vc.selectedCountry = country
        
        posts.forEach { (index) in
            // Construct a Person instance for each user in that country
            let personToSend = Person(
                name: index["username"] as! String,
                uid: index["created_by_uid"] as! String
            )
            // Construct a Resturant instance for each post in tht country
            let locationToSend = index["location"] as! [String:Any]
            let resturant = Resturant(
                name: index["place_name"] as! String,
                address: locationToSend["address"] as! String,
                postedBy:personToSend
            )
            
            let imagePath = index["images"] as? [String]
            let tempArray = imagePath![0] 
            let array = tempArray.components(separatedBy: "/")
            let tempID = array[1]
            let tempID2 = tempID.components(separatedBy: "-")
            let finalID2 = tempID2[0]
            var finalID = finalID2.dropLast()
            finalID = finalID.dropLast()
            //let array = imagePath?.components(separatedBy: "/")
            //let postID = array?[1]
            
            print("final id \(finalID)")
            
            resturant.imageRef = imagePath![0]
            vc.personArray.append(personToSend)
            vc.resturantArray.append(resturant)
            vc.postId.append(String(finalID))
            vc.postData.append(index)
        }

//        vc.personArray = [Person(name: posts[0]["headline"] as! String)]
//        vc.resturantArray = [Resturant(name: posts[0]["subtitle"] as! String, address: "Some address")]
        
        
//        vc.personArray = [Person(name: allFollowingPosts[0]["headline"] as! String)]
//        vc.resturantArray = [Resturant(name: allFollowingPosts[0]["subtitle"] as! String, address: "Some address")]

        self.present(vc, animated: true, completion: nil)
//        presentPanModal(vc)
//        posts.removeAll()
    }
    
    
    
    
    func showCallout(feature: MGLPointFeature) {
        let point = MGLPointFeature()
        point.title = feature.attributes["name"] as? String
        point.coordinate = feature.coordinate
        
        // Selecting an feature that doesn’t already exist on the map will add a new annotation view.
        // We’ll need to use the map’s delegate methods to add an empty annotation view and remove it when we’re done selecting it.
//        mapView?.selectAnnotation(point, animated: true, completionHandler: nil)
    }
    
    // MARK: - Feature interaction
    @objc @IBAction func handleMapTap(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            // Limit feature selection to just the following layer identifiers.
//            let layerIdentifiers: Set = ["countries"]

            // Try matching the exact point first.
            let point = sender.location(in: sender.view!)
//            for feature in mapView.visibleFeatures(at: point, styleLayerIdentifiers: layerIdentifiers)
//                where feature is MGLPointFeature {
//                    guard let selectedFeature = feature as? MGLPointFeature else {
//                        fatalError("Failed to cast selected feature as MGLPointFeature")
//                    }
//                    print(feature)
//                    showCallout(feature: selectedFeature)
//                    return
//            }
            
            let touchCoordinate = mapView.convert(point, toCoordinateFrom: sender.view!)
            let touchLocation = CLLocation(latitude: touchCoordinate.latitude, longitude: touchCoordinate.longitude)
//            mapView.setCenter(touchLocation.coordinate, animated: true)
            
            let geocode = Geocoder.shared
            let option = ReverseGeocodeOptions(location: touchLocation)
            option.allowedScopes = [.country]

            geocode.geocode(option) { (place, _, _) in
                guard let location = place?.first?.location?.coordinate else{return}
                print(location)
                self.search(for: location)
//                self.openSlide()
                guard let country = place?.first?.addressDictionary?["name"] else{return}
                print(country)
                self.openSlide(for: country as! String)

            }

//
//            // Otherwise, get all features within a rect the size of a touch (44x44).
//            let touchRect = CGRect(origin: point, size: .zero).insetBy(dx: -22.0, dy: -22.0)
//            let possibleFeatures = mapView.visibleFeatures(in: touchRect, styleLayerIdentifiers: Set(layerIdentifiers)).filter { $0 is MGLPointFeature }
//
//            // Select the closest feature to the touch center.
//            let closestFeatures = possibleFeatures.sorted(by: {
//                return CLLocation(latitude: $0.coordinate.latitude, longitude: $0.coordinate.longitude).distance(from: touchLocation) < CLLocation(latitude: $1.coordinate.latitude, longitude: $1.coordinate.longitude).distance(from: touchLocation)
//            })
//            if let feature = closestFeatures.first {
//                guard let closestFeature = feature as? MGLPointFeature else {
//                    fatalError("Failed to cast selected feature as MGLPointFeature")
//                }
//                showCallout(feature: closestFeature)
//                return
//            }
            
            // If no features were found, deselect the selected annotation, if any.
            //mapView.deselectAnnotation(mapView.selectedAnnotations.first, animated: true)
        }
    }
    

    
    
//    func getPosts(by user:String) -> [Any] {
//        db.collection("Trylls").whereField("created_by", isEqualTo: user)
////        { (snapshot, err) in
////            //            print(err)
////            return snapshot?.documents.map{
////                print($0.data())
////            }
////        }
//    }
    
    
    
    func loadDataFromFireStore() {
        let following : [String] = []
        let  db = Firestore.firestore()
        DispatchQueue.main.async {
            
            
            guard let uid = Auth.auth().currentUser?.uid else{
                self.showAlert(title: "Error happened", message: "Failed at downloading data for exlpore, Reason: You are not logged in,", completion: nil)
                return
            }
            
            
            
            
            // get all my following uid
            db.collection("Users").document(uid).collection("Following").getDocuments { (snapshot, err) in
                // Check for no error
                guard err == nil else{return}
//                / get every person I follow
                snapshot?.documents.forEach({ (f) in
                    let followedRef = f.data() as! [String:String]
                    print(" followed ID \(String(describing: following.values))")

                    // Get every post by each
                    Firestore.firestore().collection("Trylls").whereField("created_by_uid", isEqualTo: followedRef["user_Id"]!).getDocuments(completion: { (snapshot2, err) in
                        snapshot2?.documents.forEach({
                            let post = $0.data()
                            print(post)
                            allFollowingPosts.append(post)
                            self.filteredPost = allFollowingPosts
                            self.updateCountries(dict: self.filteredPost)
                        })
                    })

            })
                
                
                
                
                
                
//                // get every person I follow
//                snapshot?.documents.map({ (a) in
//                    let allFollowingRef = a.data() as! [String:DocumentReference]
//                    print(allFollowingRef)
//
//                    // get every post they have
//                    allFollowingRef.forEach({ (k,v) in
//                        v.collection("Trylls").getDocuments(completion: { (snapshot2, err) in
//
//                            // get each post's information
//                            snapshot2?.documents.map({ (tryllInfo) in
//                                let posts = tryllInfo.data() as! [String: DocumentReference]
//                                posts.forEach({ (k1,v1) in
//                                    v1.getDocument(completion: { (snapshot3, err) in
//                                        guard let post = snapshot3?.data()  else{return}
//                                        allFollowingPosts.append(post)
//                                        self.filteredPost = allFollowingPosts
//                                        self.updateCountries(dict: self.filteredPost)
//                                    })
//                                })
//                            })
//                        })
//                    })
//                })
            }

        
        }
        
    }
    
    
    @IBAction func refreshMap(){
        allFollowingPosts.removeAll()
        updateCountries(dict: allFollowingPosts)
        loadDataFromFireStore()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshMap()
    }
    
    fileprivate func instantiateFilterViewController() {
        filterViewController = storyboard?.instantiateViewController(withIdentifier: "FilterView") as? FilterViewController
        filterViewController.delegate = self
    }
    
    fileprivate func ConfigureMapView() {
        mapView.logoView.isHidden = true
        //        mapView.attributionButton.isHidden = true
        
        searchTextFeild.delegate = self
        mapView.delegate = self
        //        mapView.userTrackingMode = .follow
        mapView.zoomLevel = 0.3
        mapView.compassViewPosition = .bottomRight
        
        
        searchTextFeild.layer.shadowColor = UIColor.black.cgColor
        searchTextFeild.layer.shadowOffset = CGSize(width: 0, height: 0)
        searchTextFeild.layer.shadowRadius = 3
        searchTextFeild.layer.cornerRadius = 4
        searchTextFeild.layer.shadowOpacity = 0.3
        searchTextFeild.placeholder = "Search for a country..."
        searchTextFeild.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 40))
        searchTextFeild.leftViewMode = .always
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(sender:)))
        for recognizer in mapView.gestureRecognizers! where recognizer is UITapGestureRecognizer {
            singleTap.require(toFail: recognizer)
        }
        mapView.addGestureRecognizer(singleTap)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instantiateFilterViewController()
        ConfigureMapView()
        
    }
}


extension ExploreViewController: MGLMapViewDelegate{
    var source: MGLVectorTileSource{
        return MGLVectorTileSource(identifier: "ne_10m_admin_0_countries-21l0tx", configurationURL: URL(string: "mapbox://mhnd.8oy3di4r")!)


    }
    var styleUrl : URL{
        return URL(string: "mapbox://styles/mapbox/streets-v11") ?? MGLStyle.streetsStyleURL
        //return URL(string: "mapbox://styles/mhnd/cjygpcio30ot41dphsyn1wpt1/draft") ?? MGLStyle.lightStyleURL
    }
    
    func mapViewWillStartRenderingMap(_ mapView: MGLMapView) {
        print("Map Will load")
        mapView.styleURL = styleUrl
    }

//    func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {
//
//        let geocoder = Geocoder.shared
//        
//        if let coordinate = userLocation?.coordinate {
////            mapView.setCenter(coordinate, animated: false)
//            let option = ReverseGeocodeOptions(coordinate: coordinate)
//            geocoder.geocode(option) { (place, _, err) in
//                let country = place?.first?.addressDictionary?["name"] as! String
////                return country
////                mapView.setCamera(country, animated: false)
//            }
//        }
//    }
    
    
    
    func mapViewWillStartLoadingMap(_ mapView: MGLMapView) {
        // Fetch the countries and update Countries array
        // countriesArray = User.following.posts
//        countriesArray.append("India")
//        countriesArray.append("Saudi Arabia")
    }
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
       print("Did Finish Loading style")
        
        if !countriesArray.isEmpty{
            let source = MGLVectorTileSource(identifier: "ne_10m_admin_0_countries-6i91ur", configurationURL: URL(string: "mapbox://surabhi27.cjn9q7la")!)
            style.addSource(source)
            
            // Creating a layer to color
            let layer = MGLFillStyleLayer(identifier: "abc", source: source)
            let layer2 = MGLLineStyleLayer(identifier: "abcd", source: source)
            
            // Layer Source
            layer.sourceLayerIdentifier = "ne_10m_admin_0_countries-6i91ur"
            layer2.sourceLayerIdentifier = "ne_10m_admin_0_countries-6i91ur"
            
            style.addLayer(layer)
            style.addLayer(layer2)
            
            var coutriesString = ""
            countriesArray.forEach { (c) in
                if c == "United States" {coutriesString.append("(NAME == 'United States of America') || ")}
                else{coutriesString.append("(NAME == '\(c)') || ")}
            }
//            mapView.fly(to: MGLMapCamera(, completionHandler: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
            // Reforamte countires trying by erasing the last four space and delete " || "
            coutriesString = String(coutriesString.dropLast(4))

            
            layer.predicate = NSPredicate(format: coutriesString)
            layer2.predicate = NSPredicate(format: coutriesString)
            layer.fillColor = NSExpression(forConstantValue: UIColor(displayP3Red: 255/255, green: 203/255, blue: 39/255, alpha: 0.4))
            layer2.lineColor = NSExpression(forConstantValue: UIColor.tryllCountrySeperator   )
        }
       
    }
}



extension ExploreViewController:UITextFieldDelegate{
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//            self.searchTextFeild.trailingAnchor.constraint(equalTo: self.mapView.trailingAnchor,constant: -20).isActive = true
////            self.filterButton.isHidden = true
//
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//            self.searchTextFeild.trailingAnchor.constraint(equalTo: self.mapView.trailingAnchor,constant: -60).isActive  = true
        search(for: textField.text ?? "")
        textField.resignFirstResponder()
        return true
    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        UIView.animate(withDuration: 1) {
//            self.filterButton.isHidden = false
//            self.doneButton.isHidden = true
//        }
//    }
//

}












// Search bar experimental
//
//extension ExploreViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return searchedPlaces.count
//    }
//
//
//
//    static var mapbox_api = "https://api.mapbox.com/geocoding/v5/mapbox.places/"
//    static var mapbox_access_token = "pk.eyJ1IjoibWhuZCIsImEiOiJjamFvOThiNTEzajQ4MnFwbGtxaTlpN3ZqIn0.7N5tTPAHj0A9ZTJuBHvz6w"
//
//
//    @IBOutlet var tableView: UITableView!      //outlet to the tableview you created in storyboard
//    var searchActive : Bool = false  //for controlling search states
//    @IBOutlet var searchBar:UISearchBar?   //the searchbar to be added in navigation bar
//    var searchedPlaces: NSMutableArray = []   //array to store the places returned in response
//    let decoder = JSONDecoder()    //for decoding data returned by API
//
//
//
//    func searchStuff()  {
//        if self.searchBar == nil {
//            self.searchBar = UISearchBar()
//            self.searchBar!.searchBarStyle = .prominent
//            self.searchBar!.tintColor = .white
//            self.searchBar!.barTintColor = .black
//            self.searchBar!.delegate = self
//            self.searchBar!.placeholder = "Search for place";
//        }
//        self.navigationItem.titleView = searchBar
//
//    }
//
//
//
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cell")
//        cell.detailTextLabel?.textColor = UIColor.darkGray
//
//        let pred = self.searchedPlaces.object(at: indexPath.row) as! Feature
//        cell.textLabel?.text = pred.place_name!
//        if let add = pred.properties.address {
//            cell.detailTextLabel?.text = add
//        } else { }
////        cell.imageView?.image = UIImage.init(data: .fontAwesome(.mapMarker), scale: CGSize(width:30,height:30))
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60.0
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
//        let pred = self.searchedPlaces.object(at: indexPath.row) as! Feature
//        let coord = CLLocationCoordinate2D.init(latitude: pred.geometry.coordinates[1], longitude: pred.geometry.coordinates[0])
//    }
//
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        self .cancelSearching()
//        searchActive = false;
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        self.view.endEditing(true)
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        self.searchBar!.setShowsCancelButton(true, animated: true)
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        self.searchBar!.setShowsCancelButton(false, animated: false)
//    }
//
//    func cancelSearching(){
//        searchActive = false;
//        self.searchBar!.resignFirstResponder()
//        self.searchBar!.text = ""
//    }
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.searchMe), object: nil)
//        self.perform(#selector(self.searchMe), with: nil, afterDelay: 0.5)
//        if(searchBar.text!.isEmpty){
//            searchActive = false;
//        } else {
//            searchActive = true;
//        }
//    }
//
//    @objc func searchMe() {
//        if(searchBar?.text!.isEmpty)!{ } else {
//            self.searchPlaces(query: (searchBar?.text)!)
//        }
//    }
//
//    @objc func searchPlaces(query: String) {
//        let urlStr = "\(ExploreViewController.mapbox_api)\(query).json?access_token=\(ExploreViewController.mapbox_access_token)"
//
//        Alamofire.request(urlStr, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseSwiftyJSON { (dataResponse) in
//
//            if dataResponse.result.isSuccess {
//                let resJson = dataResponse.result.value!
//                if let myjson = resJson["features"].array {
//                    for itemobj in myjson ?? [] {
//                        try? print(itemobj.rawData())
//                        do {
//                            let place = try self.decoder.decode(Feature.self, from: itemobj.rawData())
//                            self.searchedPlaces.add(place)
//                            self.tableView.reloadData()
//                        } catch let error  {
//                            if let error = error as? DecodingError {
//                                print(error.errorDescription)
//                            }
//                        }
//                    }
//                }
//            }
//
//            if dataResponse.result.isFailure {
//                let error : Error = dataResponse.result.error!
//            }
//        }
//    }
//
//
//}

extension ExploreViewController: FilterViewControllerDelegate{
    func updateFilters(_ filters: [String], _ countryName: String) {
        filteredPost = allFollowingPosts.filter({  NSSet(array: filters).isSubset(of: NSSet(array: $0["tags"] as! [String]) as! Set<AnyHashable>)})
        //        filteredPost = filters.isEmpty ? allFollowingPosts : allFollowingPosts.filter({$0["tags"] as! [String] == filters})
                
                print(filters)
                print("filteredPost: \(filteredPost) country name \(countryName)")
                updateCountries(dict: filteredPost)
    }
    
    
    
    
    
    public func updateCountries(dict: [[String:Any]]){
        countriesArray.removeAll()
        dict.forEach { (post) in
            let l = post["location"] as! [String:Any]
            self.countriesArray.append(l["country"]! as! String)
        }
       self.mapView.reloadStyle(countriesArray)
    }
}


