//
//  ResturantSuggestion.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 7/30/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import Foundation
import UIKit
class Resturant: NSObject {
    
    var image:UIImage
    var imageRef: String?
    var name:String!
    var address:String!
    var postedBy:Person?
    
//    override init(){
//        self.image = #imageLiteral(resourceName: "A_small_cup_of_coffee")
//        self.name = "No name"
//        self.name = "No address"
//        self.postedBy = Person(name: "Muu",)
//    }
    
    init(name: String, address: String, image:UIImage = #imageLiteral(resourceName: "A_small_cup_of_coffee"), postedBy : Person) {
        
        self.image = image
        self.name = name
        self.address = address
        self.postedBy = postedBy
    }
    
}
