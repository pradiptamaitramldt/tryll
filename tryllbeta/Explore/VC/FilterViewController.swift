//
//  FilterViewController.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/1/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import PanModal
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import GooglePlaces
import Photos

protocol FilterViewControllerDelegate {
    func updateFilters(_ filters: [String], _ countryName : String)
}

class FilterViewController: UIViewController{
    
    
    // MARK:  IBOulets & Variables
    
    
    
    var delegate: FilterViewControllerDelegate?
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var slider: TryllSlider!
    @IBOutlet var priceTags: [UILabel]!
    @IBOutlet var filtersButtons: [TryllFilterButton]!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    // MARK:  IBAction & functions

    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applyButtonClicked(_ sender: Any) {
        
        
        let chosenFilters = filtersButtons.filter({$0.isClicked}).map({$0.buttonTitle})
        self.dismiss(animated: true, completion:{
            
            self.delegate?.updateFilters(chosenFilters, self.searchTextField.text!)
        } )
        
    }
    
    @IBAction func valueChanged(_ sender: Any) {
        guard let index = priceTags?.index(before: Int(slider.priceRange)) else{return}
        resetPriceTagLabels()
        highlightPriceTagLabel(at: index)
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.addressComponents.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // MARK:  Helpers
    func enlarge(_ label :UILabel, duration: TimeInterval, to fontSizeBig: CGFloat) {
        var biggerBounds = label.bounds
        label.font = label.font.withSize(fontSizeBig)
        biggerBounds.size = label.intrinsicContentSize
        
        label.transform = scaleTransform(from: biggerBounds.size, to: label.bounds.size)
        label.bounds = biggerBounds
        
        UIView.animate(withDuration: duration) {
            label.transform = .identity
        }
    }
    func shrink(label :UILabel, duration: TimeInterval, to fontSizeSmall: CGFloat) {
        let labelCopy = label.copyLabel()
        var smallerBounds = labelCopy.bounds
        labelCopy.font = label.font.withSize(fontSizeSmall)
        smallerBounds.size = labelCopy.intrinsicContentSize
        
        let shrinkTransform = scaleTransform(from: label.bounds.size, to: smallerBounds.size)
        
        UIView.animate(withDuration: duration, animations: {
            label.transform = shrinkTransform
        }, completion: { done in
            label.font = labelCopy.font
            label.transform = .identity
            label.bounds = smallerBounds
        })
    }


    
    func resetPriceTagLabels() {
        priceTags.forEach { (label) in
            label.textColor = .tryllBlack
            label.font = UIFont.systemFont(ofSize: 14)
//            shrink(label: label, duration: 1, to: 14)
            
        }
    }
    func highlightPriceTagLabel(at index: Int) {

        if index == 0 || index ==  1 || index ==  2 || index ==  3 {
            priceTags[index].textColor = .tryllYellow
//            enlarge(priceTags[index], duration: 0.7, to: 17)
        } else {
            return
        }
    }
    
    // MARK:  - View controller lifecycle
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    private func scaleTransform(from: CGSize, to: CGSize) -> CGAffineTransform {
        let scaleX = to.width / from.width
        let scaleY = to.height / from.height
        
        return CGAffineTransform(scaleX: scaleX, y: scaleY)
    }
}




extension UIColor{
    
    class var tryllYellow: UIColor {return UIColor(netHex: 0xFFCB27)}
    class var tryllGrey: UIColor {return UIColor(netHex: 0xededed)}
    class var tryllWhite:UIColor {return UIColor(netHex: 0xf5f5f5)}
    class var tryllBlack: UIColor {return UIColor(netHex: 0x3A3A3C)}
    class var tryllCountrySeperator: UIColor {return UIColor(netHex: 0xf2f2f2)}
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}


extension UILabel {
    func copyLabel() -> UILabel {
        let label = UILabel()
        label.font = self.font
        label.frame = self.frame
        label.text = self.text
        return label
    }
}

extension FilterViewController:PanModalPresentable{
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeightWithTopInset(50)
    }
    var shortFormHeight: PanModalHeight {
        if isViewLoaded {
            return .contentHeight(240)
        }
        return .maxHeight
    }
}



// Handle the user's selection.
extension FilterViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        self.place = place
        
//        self.searchPlace.setTitle(place.name, for: .normal)
        self.searchTextField.text = (place.addressComponents?.first(where: {$0.type == "country" })!.name)!
        
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
