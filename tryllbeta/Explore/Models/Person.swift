//
//  Person.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/5/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import Foundation
import UIKit
class Person: NSObject {
    var name: String!
    var image: UIImage
    var uid: String
    
//    override init() {
//        self.image = #imageLiteral(resourceName: "shareIcon")
//        self.name = "Tryll"
//        self.uid =
//    }
    init(name:String, uid:String, image:UIImage = #imageLiteral(resourceName: "shareIcon")){
        self.image = image
        self.name = name
        self.uid = uid
    }
}
