//
//  WDPeopleCell.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mukesh Singh on 24/09/19.
//  Copyright © 2019 OctalSoftware. All rights reserved.
//

import UIKit

class WDPeopleCell: UITableViewCell {

    @IBOutlet weak var userImageIcon: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
    
    private let imgSelected = UIImage(named: "check")
    private let imgNotSelected = UIImage(named: "uncheck")
    @IBOutlet weak var userIconLeadingConstant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkIcon.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.checkIcon.image = selected ? imgSelected : imgNotSelected
    }
    
}
