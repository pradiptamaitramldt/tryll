//
//  FriendListTableCell.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 27/03/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class FriendListTableCell: UITableViewCell {

    @IBOutlet weak var friendProfileIcon: UIImageView!
    @IBOutlet weak var buttonFollow: UIButton!
    @IBOutlet weak var labelFriendName: UILabel!
    var userUID = ""
    
    
    var isFollowed = false{
        didSet{
            buttonFollow.backgroundColor = isFollowed ? .tryllBlack :  .tryllYellow
            buttonFollow.setTitle(isFollowed ?  "Followed" : "Follow", for: .normal)
            buttonFollow.setTitleColor(isFollowed ? .tryllYellow : .tryllBlack , for: .normal)
            buttonFollow.titleLabel?.font = isFollowed ? UIFont.systemFont(ofSize: 15, weight: .bold) : UIFont.systemFont(ofSize: 15, weight: .regular)
            
        }
    }
    
    func followPerson() {
        
        let usersRef = Firestore.firestore().collection("Users")
        usersRef.document(Auth.auth().currentUser!.uid).collection("Following").addDocument(data: [
            "user_Id" : userUID
            ])
        usersRef.document(userUID).collection("Followers").addDocument(data: [
            "user_Id" : Auth.auth().currentUser!.uid
            ])
        
    }
    func UnfollowPerson(){
        let usersRef = Firestore.firestore().collection("Users")
        usersRef.document(Auth.auth().currentUser!.uid).collection("Following").whereField("user_Id", isEqualTo: userUID).getDocuments { (snap, err) in
            snap?.documents.map({ (snapshot) in
                snapshot.reference.delete()
            })
        }
        
        usersRef.document(userUID).collection("Followers").whereField("user_Id", isEqualTo: Auth.auth().currentUser!.uid).getDocuments { (snap, err) in
            snap?.documents.map({ (snapshot) in
                snapshot.reference.delete()
            })
        }
        
    }
    
    
    @objc func toggle()  {
        isFollowed.toggle()
        
        isFollowed ? followPerson() : UnfollowPerson()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        friendProfileIcon.contentMode = .scaleAspectFill
        buttonFollow.addTarget(self,action: #selector(toggle), for: .touchUpInside)
    }
    
    override func layoutSubviews() {
        friendProfileIcon.layer.cornerRadius = friendProfileIcon.frame.height / 2
        buttonFollow.layer.cornerRadius = buttonFollow.frame.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
