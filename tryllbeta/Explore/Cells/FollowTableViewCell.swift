//
//  FollowTableViewCell.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/16/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class FollowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    var userUID = ""
    
    
    var isFollowed = false{
        didSet{
            followButton.backgroundColor = isFollowed ? .tryllBlack :  .tryllYellow
            followButton.setTitle(isFollowed ?  "Followed" : "Follow", for: .normal)
            followButton.setTitleColor(isFollowed ? .tryllYellow : .tryllBlack , for: .normal)
            followButton.titleLabel?.font = isFollowed ? UIFont.systemFont(ofSize: 15, weight: .bold) : UIFont.systemFont(ofSize: 15, weight: .regular)
            
        }
    }
    func followPerson() {
        
        let usersRef = Firestore.firestore().collection("Users")
        
        usersRef.document(Auth.auth().currentUser!.uid).collection("Following").addDocument(data: [
            "user_Id" : userUID
            ])
        usersRef.document(userUID).collection("Followers").addDocument(data: [
            "user_Id" : Auth.auth().currentUser!.uid
            ])
        
    }
    func UnfollowPerson(){
        let usersRef = Firestore.firestore().collection("Users")
        
        usersRef.document(Auth.auth().currentUser!.uid).collection("Following").whereField("user_Id", isEqualTo: userUID).getDocuments { (snap, err) in
            snap?.documents.map({ (snapshot) in
                snapshot.reference.delete()
            })
        }
        
        usersRef.document(userUID).collection("Followers").whereField("user_Id", isEqualTo: Auth.auth().currentUser!.uid).getDocuments { (snap, err) in
            snap?.documents.map({ (snapshot) in
                snapshot.reference.delete()
            })
        }
        
    }
    
    
    @objc func toggle()  {
        isFollowed.toggle()
        
        isFollowed ? followPerson() : UnfollowPerson()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        avatarImageView.contentMode = .scaleAspectFill
        followButton.addTarget(self,action: #selector(toggle), for: .touchUpInside)
    }
    
    override func layoutSubviews() {
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
        followButton.layer.cornerRadius = followButton.frame.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
