//
//  PhotoCollectionCell.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 25/03/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
}
