//
//  FeedsCollectionViewCell.swift
//  tryllbeta
//
//  Created by Ashwini Prabhu on 7/14/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase

class FeedsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var postedImageDescription: UILabel!
    @IBOutlet weak var postedImageCategory: UIImageView!
    @IBOutlet weak var postedImageLocation: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var postedImageReaction: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var suitcaseButton: UIButton!
    @IBOutlet weak var postedImage: UIImageView!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var totalLikeCountBtn: UIButton!
    
    @IBOutlet weak var totalCommentsBtn: UIButton!
    var isSuitcaseSelected = false
    var postId: String = ""
    var delegate: FeedsCollectionViewCellDelegate?
    var createdByUID: String = ""
    var likeCount = NSInteger()
    var islikeButtonOn = Bool()
    
    override func layoutSubviews() {
        super.layoutSubviews();
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2.0;
        cityLabel.numberOfLines = 0
    }
    
    override func draw(_ rect: CGRect) {
        username.isUserInteractionEnabled = true
        username.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openProfile)))
    }
    
    @IBAction func suitcaseBtn_OnClick(_ sender: Any) {
        if isSuitcaseSelected {
            suitcaseButton.setImage(UIImage(named: "suitcase_unselected"), for: .normal)
            isSuitcaseSelected = false
            Firestore.firestore().collection("Trylls").document(self.postId).setData(["isSuitcaseSelected" : false], merge: true)
        } else {
            suitcaseButton.setImage(UIImage(named: "suitcase_selected"), for: .normal)
            isSuitcaseSelected = true
            Firestore.firestore().collection("Trylls").document(self.postId).setData(["isSuitcaseSelected" : true], merge: true)
        }
    }
    
    
    
    @IBAction func likeBtn_onClick(_ sender: Any) {
        if(islikeButtonOn == false){
            islikeButtonOn = true
            likeCount = likeCount + 1
            likeButton.setImage(UIImage(named:"like"), for: .normal)
            print("number of times liked: ", likeCount)
            //            likeButton.setTitle(String(likeCount), for: .normal)
            
            print("post ID \(self.postId)")
            Firestore.firestore().collection("Trylls").document(self.postId).updateData([
                "likes": FieldValue.arrayUnion([Auth.auth().currentUser?.uid ?? ""])
            ]){   err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                    
                }
            }
            
            totalLikeCountBtn.setTitle(String(likeCount), for: .normal)
        }
        else{
            islikeButtonOn = false
            while(likeCount > 0){
                likeCount = likeCount - 1
            }
            likeButton.setImage(UIImage(named:"unlike"), for: .normal)
            print("number of times disliked: ", likeCount)
            //            likeButton.setTitle(String(likeCount), for: .normal)
            print("post ID \(self.postId)")
            Firestore.firestore().collection("Trylls").document(self.postId).updateData([
                "likes": FieldValue.arrayRemove([Auth.auth().currentUser?.uid ?? ""])
            ]){   err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                    
                }
            }
            
            totalLikeCountBtn.setTitle(String(likeCount), for: .normal)
            
        }
    }
    
    var isCommentButtonOn = false
    //        if(isCommentButtonOn == false){
    //            isCommentButtonOn = true
    //            commentButton.setImage(UIImage(named:"comment_selected"), for: .normal)
    //        }
    //        else{
    //            isCommentButtonOn = false
    //            commentButton.setImage(UIImage(named:"comment"), for: .normal)
    //        }
    
    @objc func openProfile() {
        delegate?.openProfile(createdByUID: self.createdByUID)
    }
    
}


protocol FeedsCollectionViewCellDelegate {
    func openProfile(createdByUID: String)
}
