//
//  PostCollectionViewCell.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/15/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

class PostCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.contentMode = .scaleAspectFill
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
