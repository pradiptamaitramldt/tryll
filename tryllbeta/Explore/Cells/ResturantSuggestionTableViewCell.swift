//
//  TableViewCell.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 7/30/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

class ResturantSuggestionTableViewCell: UITableViewCell {

    @IBOutlet var peopleSuggested: [UIImageView]!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var resturantLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var suitcaseButton: UIImageView!
    
    var isSuicased: Bool = false
//    {
//        didSet{
//            isSuicased ?  animate(to: .tryllYellow) : animate(to: .clear)
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        isSuicased = false
        
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.suitcaseTapped))
        suitcaseButton.addGestureRecognizer(tap)
        suitcaseButton.isUserInteractionEnabled = true
        peopleSuggested.forEach { (imageView) in
            imageView.layer.cornerRadius = imageView.bounds.height / 2
        }
    }
    
//    override func layoutSubviews() {
//        backgroundImage.sd_setImage(with: <#T##StorageReference#>, placeholderImage: <#T##UIImage?#>)
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    @objc func suitcaseTapped()
    {
        if !isSuicased{
            //TODO: Save suitcase ID to user's database
            suitcaseButton.image = UIImage(named: "suitcase_fill")!
        }else{
            //TODO: Delete suitcase ID to user's database
            suitcaseButton.image = UIImage(named: "suitcase_nofill")!
        }
        isSuicased = !isSuicased
    }

}
