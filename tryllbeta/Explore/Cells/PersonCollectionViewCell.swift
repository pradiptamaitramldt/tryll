//
//  PersonTableViewCell.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 7/30/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

@IBDesignable class PersonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width/2
        nameLabel.text = "Muu"
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
