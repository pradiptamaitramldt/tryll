//
//  TryllFilterCircleButton.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/3/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

@IBDesignable class TryllFilterCircleButton: UIButton {
    
    var delegate: TryllFilterCircleButtonDelegate?
    
    var isClicked: Bool = false{
        didSet{
            backgroundColor = isClicked ?  .tryllYellow : .filterCircleInactive
        }
    }
    
    override func draw(_ rect: CGRect) {
        imageView?.contentMode = .scaleAspectFit
        imageEdgeInsets = UIEdgeInsets(top: 20,left: 20,bottom: 20,right: 20)
        addTarget(self, action: #selector(clicked(_:)), for: .touchUpInside)
    }
    
    @objc func clicked(_ button: TryllFilterCircleButton) {
        isClicked.toggle()
        delegate?.hideSelectedStackViews(buttonTitle: self.buttonTitle, isClicked: self.isClicked)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateCornerRadius()
    }
    
    @IBInspectable var buttonTitle: String = ""
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
        clipsToBounds = true
    }
    func animate(to color: UIColor)  {
        layoutIfNeeded()
            UIView.animate(withDuration: 1.0, delay: 0.0, options:[.repeat, .autoreverse], animations: {
                self.backgroundColor = color
                self.layoutIfNeeded()
            }, completion:nil)
    }
}

protocol TryllFilterCircleButtonDelegate {
    func hideSelectedStackViews(buttonTitle: String, isClicked: Bool)
}

extension UIColor{
    class var filterCircleInactive:UIColor{return UIColor(netHex: 0xF1F1F1)}
    
}
