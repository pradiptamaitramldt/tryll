//
//  TryllSlider.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/3/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

@IBDesignable class TryllSlider: UISlider {
    var priceRange: Float = 0
    var oldIndex = 0
    let  numbers = [1,2,3,4,5]
    @objc func sliderValueChanged(_ sender: Any) {
        
//        priceRange = floor(value)

        let index = Int(value + 0.5);
        setValue(Float(index), animated: true)
        priceRange = Float(numbers[index-1]) // <-- This numeric value you want
        
//        if oldIndex != index{
//            print("sliderIndex:\(index)")
//            oldIndex = index
//            priceRange = Float(index)
//        }
        
        
    }
    override func draw(_ rect: CGRect) {
        
        maximumTrackTintColor = .tryllGrey
        minimumTrackTintColor = .tryllYellow
        addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 8.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    
    
    
// Uncomment the code bellow if you want to change the slider thumb image from the inspector/storyboard
/*
    @IBInspectable var thumbImage: UIImage?{
        didSet{
            setThumbImage(thumbImage, for: .normal)
        }
    }
    @IBInspectable var thumbHighlightedImage: UIImage?{
        didSet{
            setThumbImage(thumbHighlightedImage, for: .highlighted)
        }
    }
*/

    
}
