//
//  TryllFilterButton.swift
//  tryllbeta
//
//  Created by Muhannad Alnemer on 8/3/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit

@IBDesignable class TryllFilterButton: UIButton {
    
    var isClicked: Bool = false{
        didSet{
            backgroundColor = isClicked ?  .tryllYellow : .clear
            titleLabel!.textColor = isClicked ? .white : .tryllBlack
            print(titleLabel?.text)
            
//            isClicked ?  animate(to: .tryllYellow) : animate(to: .clear)
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        layer.borderColor = UIColor(netHex: 0xC4C4C4).cgColor
        layer.borderWidth = 1
        addTarget(self, action: #selector(clicked(_:)), for: .touchUpInside)
    }
    
    @objc func clicked(_ button: TryllFilterButton) {
        isClicked.toggle()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    @IBInspectable var buttonTitle: String = ""
    @IBInspectable var rounded: Bool = true {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? 3 : 0
        clipsToBounds = true
    }
    
    func animate(to color: UIColor)  {
        UIView.animate(withDuration: 4.0) {
            self.backgroundColor =  color
        }
    }
    
    
    
}
