//
//  SuitcaseViewController.swift
//  tryllbeta
//
//  Created by Surabhi Chavan on 11/15/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import GooglePlaces
import Photos

class SuitcaseViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating {
    
    let dispatchGroup = DispatchGroup()
    var postsIds: [String] = []
    let searchController = UISearchController(searchResultsController: nil)
    
    var filteredPosts : [[String:Any]] = []
    var uidTemp = ""
    //allFollowingPosts - ALL POSTS IN THE COLLECTION VIEW
    //filteredPosts - allFollowingPosts ko filter karke
    //postsIds : document ids of the posts
    
    /* allFollowingPosts :
     // Post 1 - id : abc
     // Post 2 - id : xyz
     // Post 3 - id : ikl
     
     filteredPosts :
     // Post 3 - id : ikl
     //
     //
     
     postsIds :
     abc
     xyz
     ikl
     
     */
    
    
    func loadFromFirebase()  {
        postsIds.removeAll()
        allFollowingPosts.removeAll()
        filteredPosts.removeAll()
        DispatchQueue.main.async {
            self.feedCollectionView.reloadData()
        }
        guard let uid = Auth.auth().currentUser?.uid else{
            self.showAlert(title: "Error happened", message: "Failed at downloading data for exlpore, Reason: You are not logged in,", completion: nil)
            return
        }
        
        
        
        Firestore.firestore().collection("Users").document(uid).collection("Following").getDocuments { (snapshot, err) in
            // Check for no error
            self.uidTemp = uid
            guard err == nil else{return}
            //get every person I follow
            snapshot?.documents.map({ (f) in
                let followedRef = f.data() as! [String:String]
                // Get every post by each
                Firestore.firestore().collection("Trylls").whereField("created_by_uid", isEqualTo: followedRef["user_Id"]!).getDocuments(completion: { (snapshot2, err) in
                    snapshot2?.documents.map({
                        let post = $0.data()
                        
                        let isSuitcaseSelected = post["isSuitcaseSelected"] as? Int ?? 0
                        if  isSuitcaseSelected == 1 {
                            let documentId = $0.documentID
                            self.postsIds.append(documentId)
                            allFollowingPosts.append(post)
                            self.filteredPosts.append(post)
                            
                            DispatchQueue.main.async {
                                if self.filteredPosts.count>0{
                                    self.feedCollectionView.reloadData()
                                    self.placeholderImage.isHidden=true
                                }else{
                                    self.placeholderImage.isHidden=false
                                }
                            }
                        }
                    })
                })
//                if self.filteredPosts.count>0{
//                    self.placeholderImage.isHidden=true
//                }else{
//                    self.placeholderImage.isHidden=false
//                }
            })
        }
    }
    
    @IBOutlet var feedCollectionView: UICollectionView!
    @IBOutlet weak var placeholderImage: UIImageView!
    
    override func viewDidLoad() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search country"
        searchController.searchBar.setImage(UIImage(), for: .clear, state: .normal)
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.placeholderImage.isHidden=true
        self.navigationItem.titleView = searchController.searchBar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.feedCollectionView.backgroundColor = UIColor.tryllGrey
        feedCollectionView.delegate = self
        feedCollectionView.dataSource = self
        self.searchController.searchBar.text = ""
        self.searchController.searchBar.setShowsCancelButton(false, animated: true)
        
        loadFromFirebase()
    }
    
    @objc func deletePost(button: UIButton) {
        Firestore.firestore().collection("Trylls").document(self.postsIds[button.tag]).delete { (error) in
            self.loadFromFirebase()
        }
    }
    
    @objc func TotallikeClicked(sender:UIButton)
       {
           let vc = storyboard?.instantiateViewController(withIdentifier: "FollowSuggestionViewController") as! FollowSuggestionViewController
           vc.fromFeeds = true
           
           if let likes = filteredPosts[sender.tag]["likes"] as? [String] {
               vc.likeUser  = likes
           } else {
               vc.likeUser = []
           }
           
           self.navigationController?.pushViewController(vc, animated: true)
       }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        print(searchText)
//        filteredPosts.removeAll()
//        for value in allFollowingPosts {
//            let location =  value["location"] as! [String:String]
//            let address = location["address"]!
//            if (address.contains(searchText)) {
//                filteredPosts.append(value)
//            }
//        }
//
//        feedCollectionView.reloadData()
        if searchText.isEmpty {
            filteredPosts.removeAll()
            feedCollectionView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
        }
    }
    
    private func updateSearch(withSeaerchText searchText : String) {
        print(searchText)
        filteredPosts.removeAll()
        
        DispatchQueue.global(qos: .background).async {

            // Validate user input

            for value in allFollowingPosts {
                let location =  value["location"] as! [String:String]
                let address = location["address"]!
                if (address.contains(searchText)) {
                    self.filteredPosts.append(value)
                }
            }

            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.feedCollectionView.reloadData()
            }
        }
        
        
        
        
    }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.addressComponents.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
        
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filteredPosts.removeAll()
        feedCollectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            searchBar.resignFirstResponder()
        }
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    //to display the number of collection cells/ posts in the feed
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filteredPosts.count != 0 {
            return filteredPosts.count
        }
        return allFollowingPosts.count
    }
    
    //to display collection view cell contents
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeedsCollectionViewCell
        
        if filteredPosts.count != 0 {
            DispatchQueue.main.async {
                
                let item = self.filteredPosts[indexPath.row]
                cell.postId = self.postsIds[indexPath.row]
                let imageArray = item["images"] as! [String]
                let firstImage =  imageArray[0] as String
                let imgRef = Storage.storage().reference().child(firstImage)
                cell.postedImage.sd_setImage(with: imgRef, placeholderImage: #imageLiteral(resourceName: "Group"))
                //cell.postedImage.contentMode = .scaleAspectFit
                
                let profileImgRef = Storage.storage().reference().child("user").child(item["created_by_uid"] as! String).child("profile_photo.jpg")
                cell.profileImage.sd_setImage(with: profileImgRef)
                cell.timeStamp.text = item["timestamp"] as? String ?? ""
                cell.username.text = item["username"] as? String
                let location =  item["location"] as! [String:String]
                cell.cityLabel.text = location["address"]!
                cell.postedImageLocation.text = item["place_name"] as? String
                cell.postedImageDescription.text = item["headline"] as? String
                cell.postedImageDescription.numberOfLines=0
                cell.postedImageDescription.lineBreakMode = .byCharWrapping
                cell.postedImageDescription.sizeToFit()
                let postTimeStr = item["timestamp"] as? String
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
                let postTime = formatter.date(from: postTimeStr ?? "")
                let time = postTime?.timeAgoDisplay()
                print("timeAgo ... \(time ?? "NA")")
                cell.timeStamp.text = time//item["timestamp"] as? String
                cell.timeStamp.isHidden = false
                //cell.timeStamp.text =   item["timestamp"] as? String
                cell.postedImageReaction.image = updateEmojiRatingCatagory(item["emojiRating"] as! Int)
                if let tags = item["tags"] as? [String]{
                    cell.postedImageCategory.image = updateImageCatagory(tags)
                }
                
                
                if let totallike = item["likes"] as? [String]{
                    print("total likes \(totallike.count)")
                    cell.likeCount = totallike.count
                    
                    
                    if totallike.count > 0 {
                        cell.totalLikeCountBtn.setTitle(String(totallike.count), for: .normal)
                        if totallike.contains(self.uidTemp) {
                            print("post id \(self.postsIds[indexPath.row])")
                            print("indexpath \(indexPath.row) like section")
                            cell.islikeButtonOn = true
                            cell.likeButton.setImage(UIImage(named:"like"), for: .normal)
                            //cell.likeButton.addTarget(self,action:#selector(self.unlikeClicked), for:.touchUpInside)
                            
                        } else {
                            print("indexpath \(indexPath.row) unlike section")
                            cell.islikeButtonOn = false
                            cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                            //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                        }
                    } else {
                        cell.likeCount = 0
                        cell.islikeButtonOn = false
                        cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                        cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                    }
                    
                    
                                   
                } else {
                        cell.likeCount = 0
                        cell.islikeButtonOn = false
                        cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                        cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                   // cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                }
                
                
                cell.likeButton.tag = indexPath.row
                
                
                cell.totalLikeCountBtn.addTarget(self,action:#selector(self.TotallikeClicked), for:.touchUpInside)
                
                if let totalComments = item["comments"] as? [[String : Any]] {
                    cell.totalCommentsBtn.setTitle(String(totalComments.count), for: .normal)
                } else {
                    cell.totalCommentsBtn.setTitle("", for: .normal)
                }
                
                
                cell.buttonDelete.addTarget(self, action: #selector(self.deletePost(button:)), for: .touchUpInside)
                cell.buttonDelete.tag = indexPath.row
            }
        }
        else {
            
            DispatchQueue.main.async {
                
                let item = allFollowingPosts[indexPath.row]
                cell.postId = self.postsIds[indexPath.row]
                let imgRef = Storage.storage().reference().child(item["images"] as! String)
                cell.postedImage.sd_setImage(with: imgRef, placeholderImage: #imageLiteral(resourceName: "Group"))
                cell.postedImage.contentMode = .scaleAspectFit
                
                let profileImgRef = Storage.storage().reference().child("user").child(item["created_by_uid"] as! String).child("profile_photo.jpg")
                cell.profileImage.sd_setImage(with: profileImgRef)
                cell.timeStamp.text = item["timestamp"] as? String ?? ""
                cell.username.text = item["username"] as? String
                let location =  item["location"] as! [String:String]
                cell.cityLabel.text = location["address"]!
                cell.postedImageLocation.text = item["place_name"] as? String
                cell.postedImageDescription.text = item["headline"] as? String
                cell.postedImageDescription.numberOfLines=0
                cell.postedImageDescription.lineBreakMode = .byCharWrapping
                cell.postedImageDescription.sizeToFit()
                //cell.timeStamp.text =   item["timestamp"] as? String
                let postTimeStr = item["timestamp"] as? String
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
                let postTime = formatter.date(from: postTimeStr ?? "")
                let time = postTime?.timeAgoDisplay()
                print("timeAgo ... \(time ?? "NA")")
                cell.timeStamp.text = time//item["timestamp"] as? String
                cell.timeStamp.isHidden = false
                cell.postedImageReaction.image = updateEmojiRatingCatagory(item["emojiRating"] as! Int)
                if let tags = item["tags"] as? [String]{
                    cell.postedImageCategory.image = updateImageCatagory(tags)
                }
                
                
                if let totallike = item["likes"] as? [String]{
                    print("total likes \(totallike.count)")
                    cell.likeCount = totallike.count
                    
                    
                    if totallike.count > 0 {
                        cell.totalLikeCountBtn.setTitle(String(totallike.count), for: .normal)
                        if totallike.contains(self.uidTemp) {
                            print("post id \(self.postsIds[indexPath.row])")
                            print("indexpath \(indexPath.row) like section")
                            cell.islikeButtonOn = true
                            cell.likeButton.setImage(UIImage(named:"like"), for: .normal)
                            //cell.likeButton.addTarget(self,action:#selector(self.unlikeClicked), for:.touchUpInside)
                            
                        } else {
                            print("indexpath \(indexPath.row) unlike section")
                            cell.islikeButtonOn = false
                            cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                            //cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                        }
                    } else {
                        cell.likeCount = 0
                        cell.islikeButtonOn = false
                        cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                        cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                    }
                    
                    
                                   
                } else {
                        cell.likeCount = 0
                        cell.islikeButtonOn = false
                        cell.likeButton.setImage(UIImage(named:"unlike"), for: .normal)
                        cell.totalLikeCountBtn.setTitle(String(""), for: .normal)
                   // cell.likeButton.addTarget(self,action:#selector(self.likeClicked), for:.touchUpInside)
                }
                
                
                cell.likeButton.tag = indexPath.row
                
                cell.totalLikeCountBtn.addTarget(self,action:#selector(self.TotallikeClicked), for:.touchUpInside)
                
                if let totalComments = item["comments"] as? [[String : Any]] {
                    cell.totalCommentsBtn.setTitle(String(totalComments.count), for: .normal)
                } else {
                    cell.totalCommentsBtn.setTitle("", for: .normal)
                }
                
                cell.buttonDelete.addTarget(self, action: #selector(self.deletePost(button:)), for: .touchUpInside)
                cell.buttonDelete.tag = indexPath.row
            }
        }
        return cell
    }
    
    // unwinds to feed view controller on clicking back button of fullImage View controller
    @IBAction func UnwindToFeedView(unwindSegue: UIStoryboardSegue){
        print("Pressed Back button")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 520)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    //Triggered when user taps on the post
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FullImageViewController") as! FullImageViewController
        self.navigationController?.navigationBar.tintColor = .tryllBlack
        var item : [String:Any] = [:]
        if filteredPosts.count != 0 {
            item = filteredPosts[indexPath.row]
        } else {
            item = allFollowingPosts[indexPath.row]
        }
        let itemId = postsIds[indexPath.row]
        
        //let imgRef = Storage.storage().reference().child(item["images"] as! String)
        vc.postData = item
        vc.postId = itemId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}



// Handle the user's selection.
extension SuitcaseViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        self.place = place
        
//        self.searchPlace.setTitle(place.name, for: .normal)
        self.searchController.searchBar.text = (place.addressComponents?.first(where: {$0.type == "country" })!.name)!
        
        
        
        self.updateSearch(withSeaerchText: (place.addressComponents?.first(where: {$0.type == "country" })!.name)!)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
