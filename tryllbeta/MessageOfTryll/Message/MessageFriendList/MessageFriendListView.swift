//
//  MessageFriendListView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 14/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import FirebaseUI
import FirebaseAuth
import FirebaseFirestore



class MessageFriendListView: UIViewController {

    @IBOutlet weak var table_view: UITableView!
    var dbRef:DatabaseReference!
    let userid = Auth.auth().currentUser?.uid
    var usersUidList:[String]! = []
    var users: [[String: Any]] = [[:]]
    var filteredUsers: [[String: Any]] = [[:]]
    var lastMessageList: [ChatModel]! = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.table_view.delegate=self
        self.table_view.dataSource=self
        usersUidList.removeAll()
        filteredUsers.removeAll()
        dbRef = Database.database().reference()
        getChatList()
    }
    
    func getChatList(){
        dbRef.child("chat").child(userid!).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.childrenCount > 0{
                for child in snapshot.children{
                    let child = child as! DataSnapshot
                    let postDict = child.key as? String ?? ""
                    self.usersUidList.append(postDict)
                    print("chated user \(self.usersUidList)")
                }
                self.messageUsers()
            }
        }
    }
    
    func messageUsers() {
           
        if usersUidList.count > 0 {
            for user in usersUidList {
                if user != Auth.auth().currentUser!.uid {
                    Firestore.firestore().collection("Users").document(user).getDocument { (snap, error) in
                        guard snap?.data() != nil else{return}
                        print(snap?.data())
                        let dict = snap?.data() as! [String:Any]
                         self.users.append(dict)
                         self.filteredUsers.append(dict)
                        
                         self.table_view.reloadData()
                    }
                }
            }
            
        }
           
    }
    
    
    func getLastMessage(){
        if usersUidList.count > 0{
        for uid in usersUidList{
            
        dbRef.child("chat").child(userid!).child(uid).child("messages").queryOrderedByKey().queryLimited(toLast: 1).observeSingleEvent(of: .value, with: { (snapshot) in
            
                if snapshot.childrenCount > 0{
                    for child in snapshot.children{
                        let child = child as! DataSnapshot
                        let postDict = child.value as? [String:Any] ?? [:]
                        let chat = ChatModel(fromDictionary: postDict)
                        self.lastMessageList.append(chat)
                    }
                }
            
                print("Last chat \(self.lastMessageList.count)")

            })
            
           // print("Last chat \(self.lastMessageList.count)")
           // getDetails(uid: uid)
            self.messageUsers()
        }
        }
    }
    
    
    
    
    @IBAction func buttonBackAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSearchAction(_ sender: Any) {
    }
}
extension MessageFriendListView : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageFriendListTableCell", for: indexPath) as! MessageFriendListTableCell
        
        if let userPhotoRef = filteredUsers[indexPath.row]["profile_picture"] {
            let photoRef = Storage.storage().reference().child(userPhotoRef as! String)
            cell.imageViewProfile.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
        }
        cell.labelUserName.text = filteredUsers[indexPath.row]["username"] as? String ?? "..."
        //cell.labelMessage.text = lastMessageList[indexPath.row].message ?? ""
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Message", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChatView") as! ChatView
        //vc.hidesBottomBarWhenPushed = true
        vc.reciverID = filteredUsers[indexPath.row]["uid"] as! String
        vc.reciverImageLink = filteredUsers[indexPath.row]["profile_picture"] as! String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
