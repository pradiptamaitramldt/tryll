//
//  CustomChatTextField.swift
//  MARITIME YACHT SERVICES
//
//  Created by Pradipta on 13/11/19.
//  Copyright © 2019 OctalSoftware. All rights reserved.
//

import UIKit

class CustomChatTextField: UITextField {
    var lang: String = ""
    private func getKeyboardLanguage() -> String? {
        return lang // here you can choose keyboard any way you need
    }
    
    override var textInputMode: UITextInputMode? {
        if let language = getKeyboardLanguage() {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
        }
        return super.textInputMode
    }
}
