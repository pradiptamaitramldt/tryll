//
//  ChatReceiverTableCell.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 15/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit

class ChatReceiverTableCell: UITableViewCell {

    @IBOutlet weak var imageViewProfileIcon: UIImageView!
    @IBOutlet weak var textViewReceiverText: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
