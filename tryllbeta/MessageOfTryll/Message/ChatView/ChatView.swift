//
//  ChatView.swift
//  tryllbeta
//
//  Created by Pradipta Maitra on 15/04/20.
//  Copyright © 2020 Tryll. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseFirestore
import Firebase
import Foundation
import FirebaseStorage

class ChatView: UIViewController {

    @IBOutlet weak var buttonUserImage: UIBarButtonItem!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var textFieldChat: CustomChatTextField!
    var dbRef:DatabaseReference!
    var reciverID = ""
    var reciverImageLink = ""
    var senderID = Auth.auth().currentUser?.uid
    var chatList:[ChatModel]! = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dbRef = Database.database().reference()
        self.table_view.delegate=self
        self.table_view.dataSource=self
        self.table_view.estimatedRowHeight = UITableView.automaticDimension
        self.table_view.rowHeight = 40.0
        self.textFieldChat.delegate=self
        getChatList(recievrUID: reciverID)
    }
    
    func dateWithSlash(date:Date)-> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/MM/dd/h/mm/a"
    //    let dateStr = formatter.string(from: date)
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let dateStr = formatter.string(from: date)
        return dateStr
    }
    
    func getChatList(recievrUID:String){
           dbRef.child("chat").child(senderID!).child(recievrUID).child("messages").observe(.value) { (snapshot) in
               if snapshot.childrenCount > 0{
                   self.chatList.removeAll()
                   for child in snapshot.children{
                       let child = child as! DataSnapshot
                       let postDict = child.value as? [String:Any]  ?? [:]
                       let chat = ChatModel(fromDictionary: postDict)
                       self.chatList.append(chat)
                   }
                   if self.chatList.count > 0{
                       self.chatList.insert(ChatModel(fromDictionary: [:]), at: 0)
                       for i in 1..<self.chatList.count{
                           if self.chatList[i].timestamp != nil && self.chatList[i-1].timestamp != nil {
                               if !self.splitDate(dateStr: self.chatList[i].timestamp).contains(self.splitDate(dateStr: self.chatList[i-1].timestamp)){
                                   self.chatList.insert(ChatModel(fromDictionary: [:]), at: i)
                               }
                           }
                       }}
                self.table_view.scroll(to: .bottom, animated: true)
                self.table_view.reloadData()
               }
            self.table_view.scroll(to: .bottom, animated: true)
           }
       }
    
    func splitDate(dateStr:String)->String{
        
        let dateArr = dateStr.components(separatedBy: "/")
        let year = dateArr[0]
        let month = dateArr[1]
        let day = dateArr[2]
        let date = month+"/"+day+"/"+year
        return date
    }
    
    func splitTime(dateStr:String)->String{
        let dateArr = dateStr.components(separatedBy: "/")
        let hour = dateArr[3]
        let minute = dateArr[4]
        let second = dateArr[5]
        let time = hour+":"+minute+" "+second
        return time
    }
    

    private func sendMessage() {
        let key = dbRef.child("chat").child(senderID!).child(reciverID).child("messages").childByAutoId().key
        let message = textFieldChat.text
        if !(message?.isEmpty)! {
            var chat = ChatModel(fromDictionary: [:])
            chat.destination = reciverID
            chat.message = message
            chat.sender = senderID
            chat.timestamp = dateWithSlash(date: Date())
            
            dbRef.child("chat").child(senderID!).child(reciverID).child("messages").child(key!).setValue(chat.toDictionary(), withCompletionBlock: { (error, reference) in
                    guard let error = error else {
                        return
                    }
                    
                })
            
            let key2 = dbRef.child("chat").child(reciverID).child(senderID!).child("messages").childByAutoId().key
            
            dbRef.child("chat").child(reciverID).child(senderID!).child("messages").child(key2!).setValue(chat.toDictionary(), withCompletionBlock: { (error, reference) in
                    guard let error = error else {
                        return
                    }
                    
                })
        }
    }
    
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSendChatAction(_ sender: Any) {
        sendMessage()
        textFieldChat.resignFirstResponder()
        textFieldChat.text = ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.table_view.reloadData()
        }
        
        
    }
    
}
extension ChatView : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reciverCell = tableView.dequeueReusableCell(withIdentifier: "chatReceiverTableCell", for: indexPath) as! ChatReceiverTableCell
         let senderCell = tableView.dequeueReusableCell(withIdentifier: "chatSenderTableCell", for: indexPath) as! ChatSenderTableCell
        if (chatList[indexPath.row].sender == senderID!) {
            
            reciverCell.selectionStyle = UITableViewCell.SelectionStyle.none
            reciverCell.textViewReceiverText.clipsToBounds = true
            reciverCell.textViewReceiverText.layer.cornerRadius = 10
            reciverCell.textViewReceiverText.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            reciverCell.textViewReceiverText.text = chatList[indexPath.row].message
            reciverCell.textViewReceiverText.textContainerInset = UIEdgeInsets(top: 15, left: 10, bottom: 15, right: 10)
            let photoRef = Storage.storage().reference().child("user/\(senderID!)/profile_photo.jpg")
            reciverCell.imageViewProfileIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            
//            let photoRef = Storage.storage().reference().child(reciverImageLink as! String)
//            reciverCell.imageViewProfileIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            return reciverCell
        }
        else{
            senderCell.selectionStyle = UITableViewCell.SelectionStyle.none
            senderCell.textViewSenderText.clipsToBounds = true
            senderCell.textViewSenderText.layer.cornerRadius = 10
            senderCell.textViewSenderText.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            senderCell.textViewSenderText.text = chatList[indexPath.row].message
            senderCell.textViewSenderText.textContainerInset = UIEdgeInsets(top: 15, left: 10, bottom: 15, right: 10)
            let photoRef = Storage.storage().reference().child(reciverImageLink as! String)
            senderCell.imageViewProfileIcon.sd_setImage(with: photoRef, placeholderImage: #imageLiteral(resourceName: "Group 6"))
            return senderCell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension ChatView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if isEmojiTapped==false {
//            self.textFieldChat.lang="en"
//        }else{
//            self.textFieldChat.lang="emoji"
//        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.isEmojiTapped=false
    }
}





struct ChatModel{
    
    var sender : String!
    var destination : String!
    var message : String!
    var timestamp : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        sender = dictionary["sender"] as? String
        message = dictionary["message"] as? String
        timestamp = dictionary["timestamp"] as? String
        destination = dictionary["destination"] as? String

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if sender != nil{
            dictionary["sender"] = sender
        }
        if message != nil{
            dictionary["message"] = message
        }
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
        if destination != nil{
            dictionary["destination"] = destination
        }
       
        return dictionary
    }

}
