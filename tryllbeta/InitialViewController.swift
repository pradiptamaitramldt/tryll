//
//  SignUpViewController.swift
//  tryllbeta
//
//  Created by Tryll on 6/27/19.
//  Copyright © 2019 Tryll. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FirebaseAuth
import Firebase
import GoogleSignIn

class InitialViewController: UIViewController {
    let userDefault = UserDefaults.standard
    var facebookDetails = [String : AnyObject]()
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        DispatchQueue.main.async {
            if Auth.auth().currentUser != nil && defaults.bool(forKey: "usersignedin"){
                self.performSegue(withIdentifier: "segueToTabBarViewContoller", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    @IBAction func buttonFBActtion(_ sender: UIButton) {
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        let fbLoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
               if let error = error {
                   print("Failed to login: \(error.localizedDescription)")
                self.activityIndicatorView.stopAnimating()
                   return
               }
               
            guard let accessToken = AccessToken.current else {
                   print("Failed to get access token")
                self.activityIndicatorView.stopAnimating()
                   return
               }
            
            if result!.isCancelled {
                self.activityIndicatorView.stopAnimating()
            } else {
                
            }
            
        
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
               
               // Perform login by calling Firebase APIs
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                   if let error = error {
                       print("Login error: \(error.localizedDescription)")
                       let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                       let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                       alertController.addAction(okayAction)
                       self.present(alertController, animated: true, completion: nil)
                       
                       return
                   }
                print(user as Any)
                print(user?.additionalUserInfo as Any)
                print(user?.credential as Any)
                self.getFBUserData()
                if (user?.additionalUserInfo!.isNewUser)! {
                    
                } else {
                    //self.pushToHome()
                }
                
                
                   // Present the main view
//                   if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainView") {
//                       UIApplication.shared.keyWindow?.rootViewController = viewController
//                       self.dismiss(animated: true, completion: nil)
//                   }
                   
               })
        
           }
    }
    @IBAction func buttonGoogleAction(_ sender: UIButton) {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func getFBUserData(){
     if((AccessToken.current) != nil){
         GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.facebookDetails = result as! [String : AnyObject]
                    print(result!)
                    print(self.facebookDetails)
                    self.firstTimeLoginSetupwithFacebook(withfacebookData: self.facebookDetails)
                 //self.loginApi(withLoginType: "FACEBOOK", withUserText: self.facebookDetails["id"] as! String)
                }
            })
        }
    }
    
    
    
    private func firstTimeLoginSetupwithFacebook(withfacebookData facebbokdata : [String : AnyObject]) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let picture = self.facebookDetails["picture"] as! [String : Any]
        let picdata = picture["data"] as! [String : Any]
        getImageFromWeb(picdata["url"] as! String) { (image) in
            if let image = image {
               let storageRef = Storage.storage().reference().child("user/\(uid)/profile_photo.jpg")
               
               guard let imageData = image.jpegData(compressionQuality: 0.75) else { return }
               
               let metaData = StorageMetadata()
               metaData.contentType = "image/JPEG"
               
               storageRef.putData(imageData, metadata: metaData) { metaData, error in
                   if error == nil, metaData != nil {
                       storageRef.downloadURL() { url, error in
                           let userObject : [String:Any] = [
                                  "uid":uid,
                                  "username": self.facebookDetails["name"] as! String,
                                  "profile_picture": "user/\(uid)/profile_photo.jpg",
                                  "first_name" : "",
                                  "last_name" : "",
                                  "phone" : "",
                                  "email" : self.facebookDetails["email"] as! String,
                                  "bio" : "",
                                  "profileType": "Public"
                                  ]
                                  
                    Firestore.firestore().collection("Users").document(uid).setData(userObject)
                        self.activityIndicatorView.stopAnimating()
                        self.pushToHome()
                       }
                   } else {
                       
                       //failed!
                       
                   }
               }
            }
        }  
    }
    
    
    private func firstTimeLoginWithGoooleLogin(withUserName userName : String, withEmailID emailId : String,withProfileImage profileImage : String) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let picdata = profileImage
        getImageFromWeb(picdata) { (image) in
            if let image = image {
               let storageRef = Storage.storage().reference().child("user/\(uid)/profile_photo.jpg")
               
               guard let imageData = image.jpegData(compressionQuality: 0.75) else { return }
               
               let metaData = StorageMetadata()
               metaData.contentType = "image/JPEG"
               
               storageRef.putData(imageData, metadata: metaData) { metaData, error in
                   if error == nil, metaData != nil {
                       storageRef.downloadURL() { url, error in
                           let userObject : [String:Any] = [
                                  "uid":uid,
                                  "username": userName,
                                  "profile_picture": "user/\(uid)/profile_photo.jpg",
                                  "first_name" : "",
                                  "last_name" : "",
                                  "phone" : "",
                                  "email" : emailId,
                                  "bio" : "",
                                  "profileType": "Public"
                                  ]
                                  
                    Firestore.firestore().collection("Users").document(uid).setData(userObject)
                        self.activityIndicatorView.stopAnimating()
                        self.pushToHome()
                       }
                   } else {
                       
                       //failed!
                       
                   }
               }
            }
        }
        
        
       
        
    }
    
    
    private func pushToHome() {
        self.userDefault.set(true, forKey: "usersignedin")
        self.userDefault.synchronize()
        self.removeSpinner()
        self.performSegue(withIdentifier: "segueToTabBarViewContoller", sender: self)
    }
    
    
    func getImageFromWeb(_ urlString: String, closure: @escaping (UIImage?) -> ()) {
            guard let url = URL(string: urlString) else {
    return closure(nil)
            }
            let task = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                guard error == nil else {
                    print("error: \(String(describing: error))")
                    return closure(nil)
                }
                guard response != nil else {
                    print("no response")
                    return closure(nil)
                }
                guard data != nil else {
                    print("no data")
                    return closure(nil)
                }
                DispatchQueue.main.async {
                    closure(UIImage(data: data!))
                }
            }; task.resume()
        }
    
}


private typealias GoogleDelegates = InitialViewController
extension GoogleDelegates : GIDSignInDelegate {
    //MARK:Google SignIn Delegate
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }

    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    

    //completed sign In
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {

        if (error == nil) {
            let googleUser = user
            // Perform any operations on signed in user here.
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
                     // Perform login by calling Firebase APIs
                        Auth.auth().signIn(with: credential, completion: { (user, error) in
                               if let error = error {
                                   print("Login error: \(error.localizedDescription)")
                                   let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                                   let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                   alertController.addAction(okayAction)
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   return
                               }
                            print(user as Any)
                            print(user?.additionalUserInfo as Any)
                            print(user?.credential as Any)
                            let fullName = googleUser?.profile.name!
                            let email = googleUser?.profile.email!
                            let profileImage = googleUser?.profile.imageURL(withDimension: 100*100)
                            
                            self.firstTimeLoginWithGoooleLogin(withUserName: fullName!, withEmailID: email!, withProfileImage: profileImage!.absoluteString)
                            //self.pushToHome()
                               // Present the main view
            //                   if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainView") {
            //                       UIApplication.shared.keyWindow?.rootViewController = viewController
            //                       self.dismiss(animated: true, completion: nil)
            //                   }
                               
                           })
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
}
